// import de modulos
var express         = require('express'),
    session         = require('express-session'),
    cookieParser    = require('cookie-parser'),
    bodyParser      = require('body-parser'),
    swig            = require('swig'),
    _               = require('underscore'),
    passport        = require('passport'),
    flash           = require('connect-flash'),
    socketio        = require("./app/config/main.socket");

// utilizacion del framework Express y Socket.io
var app     = express();
var server  = require('http').Server(app);
var io      = require('socket.io')(server);
//app.http().io();

// se carga configuracion de Passport para
// control de sesiones y usuarios
require('./app/config/passport')(passport);

// se carga base de datos no-sql para almacenar
// sesiones (en este motor de base de datos de documentos
// se controlaran las sesiones de usuario)
var RedisStore = require('connect-redis')(session);

// se define el framework de swig
// no almacene en cache las paginas html
swig.setDefaults({
   cache : false
});

// se carga como predefinido el framework de html
// a swig para controlar el renderizado de paginas html
app.engine('html', swig.renderFile);
app.set('view engine','html');
app.set('views','./app/views');

// static files (javascript, css, img)
app.use(express.static('./app/public'));

//app.use( express.logger() );
app.use(bodyParser.urlencoded({extended: true})); // obtiene informacion de los templates html
app.use(bodyParser.json()); // obtiene informacion de los templates html
app.use(cookieParser()); // lee cookies (necesarias para la utentificacion)

// configuracion de sesion en Express y las sesiones se guardaran en
// redis-server(base de datos no-sql para las sesiones)
app.use(session({
    secret   : 'ilovescotchscotchyscotchscotch',
    resave: true,
    saveUninitialized: true,
    store    : new RedisStore({})
}) );

// se le indica a la aplicacion que ocupe passport para la
// autentificacion de los usuarios
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

// CORS (Cross-Origin Resource Sharing) headers to support Cross-site HTTP requests
app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

// rutas
var rutas = require('./app/config/routes');
// valores de ruta
rutas(app, passport, io);

// funciones socket.io
socketio(app, passport, io);

server.listen(process.env.PORT || 3000, process.env.IP || '0.0.0.0');
console.log('Servidor corriendo');
