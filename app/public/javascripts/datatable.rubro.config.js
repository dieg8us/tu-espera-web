$(document).ready(function() {

      var dRubros = $('#dtRubros').dataTable({
                    bFilter: true,
                    bLengthChange: false,
                    bProcessing: true,
                    bServerSide: true,
                    bSort:       false,
                    sAjaxSource: "/getrubros",
                    iDisplayLength: 5,
                    oLanguage: {
                        "sLengthMenu": "Mostrando _MENU_ datos por pagina",
                        "sZeroRecords": "Nada encontrado",
                        "sInfo": "Monstrando _START_ de _END_ de _TOTAL_ datos",
                        "sInfoFiltered": "(filtrado de _MAX_ datos totales)",
                        "sInfoEmpty": "Mostrando 0 de 0 de 0 datos",
                        "sSearch": "",
                        "oPaginate": {
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        }
                    },
                    aoColumns: [
                      { mData: "nombre"},
                      { mData: "tipo"},
                      { mData: "flagEliminado", bVisible: false},
                      { mData: null ,
                        mRender: function(data, type, full){
                          return '<a href="#" data-toggle="modal" data-target="#updModal" class="edit" >actualizar</a>'
                        }
                      },
                      { mData: null,
                        mRender: function(data, type, full){
                          return '<a href="/eliminarRubro/' + full._id + '">eliminar</a>'
                        }
                      }
                    ],
                    fnDrawCallback: function( oSettings ) {
                      $('.dataTables_info').addClass("control-label");
                      $('.dataTables_filter input').attr('placeholder','Buscar');
                    },
                    fnServerParams: function(aoData) {
                      aoData.push({
                        name: "bChunkSearch", value: true
                      }); }
                  }).columnFilter();

    $('#dtRubros').on('click', '.edit' ,function(e){
        // datos de la fila
        var fila = e.currentTarget.parentElement.parentElement.cells[0];
        // posicion de la fila
        var aPos = dRubros.fnGetPosition( fila );
        // datos de la fila
        var aData = dRubros.fnGetData( aPos[0] );

        // nombre rubro
        var nombreRubro = aData.nombre;
        // descripcion
        var descripcionRubro = aData.tipo;

        // id Rubro
        var idRubro = aData._id;

        $('#updNombre').val(nombreRubro);
        $('#updDescripcion').val(descripcionRubro);
        $('#idRubro').val(idRubro);
    });

  });
