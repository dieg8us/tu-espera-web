$("#form-ejecutivo").validate({
    rules: {
        nombre: {
            minlength: 4,
            maxlength: 15,
            required: true
        },
        email: {
            minlength: 15,
            maxlength: 60,
            required: true,
            email: true
        },
        apellido: {
            minlength: 4,
            maxlength: 15,
        },
        usuario:{
            minlength: 4,
            maxlength: 15,
            required: true
        },
        contrasena: {
            required: true
        },
        contrasenatwo: {
          equalTo: '#contrasena',
          required: true
        },
        perfil: {
          required: true
        },
        empresa: {
          required: true
        }
    },
    messages: {
        nombre: {
          required: "Nombre requerido",
          minlength: "Ingresa minimo 4 caracteres",
          maxlength: "Ingresa maximo 15 carateres"
        },
        email: {
          required: "Email requerido",
          minlength: "Ingresa minimo 15 carateres",
          maxlength: "Ingresa maximo 60 carateres",
          email: "Favor ingresar una dirección de correo valida"
        },
        usuario: {
          minlength: "Ingrese minimo 4 caracteres",
          maxlength: "Ingrese maximo 15 caracteres",
          required: "Usuario requerido"
        },
        apellido: {
          minlength: "Ingrese minimo 4 caracteres",
          maxlength: "Ingrese maximo 15 carateres",
        },
        contrasena:{
          required: "Contraseña requerida"
        },
        contrasenatwo: {
          equalTo: "Ingresar la misma contraseña",
          required: "Contraseña de validación requerida"
        },
        perfil: {
          required: "Perfil requerido"
        },
        empresa: {
          required: "Empresa requerida"
        }

    },
    showErrors: function(errorMap, errorList) {

      $.each(this.successList, function(index, value) {
        return $(value).popover("hide");
      });

      return $.each(errorList, function(index, value) {
        var _popover;
        _popover = $(value.element).popover({
          trigger: "manual",
          placement: "left",
          content: value.message,
          template: "<div class=\"popover\"><div class=\"arrow\"></div><div class=\"popover-inner\"><div class=\"popover-content\"><p></p></div></div></div>"
        });

        // Bootstrap 3.x :
        _popover.data("bs.popover").options.content = value.message;
        // Bootstrap 2.x :
        //_popover.data("popover").options.content = value.message;
        return $(value.element).popover("show");
      });
    }
});

$("#form-upd-ejecutivo").validate({
    rules: {
        updNombre: {
            minlength: 4,
            maxlength: 15,
            required: true
        },
        updEmail: {
            minlength: 15,
            maxlength: 60,
            required: true,
            email: true
        },
        updApellido: {
            minlength: 4,
            maxlength: 15,
        }
    },
    messages: {
        updNombre: {
          required: "Nombre requerido",
          minlength: "Ingresa minimo 4 caracteres",
          maxlength: "Ingresa maximo 15 carateres"
        },
        updEmail: {
          required: "Email requerido",
          minlength: "Ingresa minimo 15 carateres",
          maxlength: "Ingresa maximo 60 carateres",
          email: "Favor ingresar una dirección de correo valida"
        },
        updApellido: {
          minlength: "Ingrese minimo 4 caracteres",
          maxlength: "Ingrese maximo 15 carateres",
        }
    },
    showErrors: function(errorMap, errorList) {

      $.each(this.successList, function(index, value) {
        return $(value).popover("hide");
      });

      return $.each(errorList, function(index, value) {
        var _popover;
        _popover = $(value.element).popover({
          trigger: "manual",
          placement: "left",
          content: value.message,
          template: "<div class=\"popover\"><div class=\"arrow\"></div><div class=\"popover-inner\"><div class=\"popover-content\"><p></p></div></div></div>"
        });

        // Bootstrap 3.x :
        _popover.data("bs.popover").options.content = value.message;
        // Bootstrap 2.x :
        //_popover.data("popover").options.content = value.message;
        return $(value.element).popover("show");
      });
    }
});

$.validator.addMethod("rut", function(value, element) {
  return this.optional(element) || $.Rut.validar(value);
}, "Este campo debe ser un rut valido" );

$("#jq-validation").validate();

// $('body').on('click', function (e) {
//       return $(this).popover("hide");
//     }
// });
