$(document).ready(function() {

      var dPertfiles = $('#dtEjecutivos').dataTable({
                    bFilter: true,
                    bLengthChange: false,
                    bProcessing: true,
                    bServerSide: true,
                    bSort:       false,
                    sAjaxSource: "/getejecutivos",
                    iDisplayLength: 4,
                    oLanguage: {
                        "sLengthMenu": "Mostrando _MENU_ datos por pagina",
                        "sZeroRecords": "Nada encontrado",
                        "sInfo": "Monstrando _START_ de _END_ de _TOTAL_ datos",
                        "sInfoFiltered": "(filtrado de _MAX_ datos totales)",
                        "sInfoEmpty": "Mostrando 0 de 0 de 0 datos",
                        "sSearch": "",
                        "oPaginate": {
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        }
                    },
                    aoColumns: [
                      { mData: "nombre"},
                      { mData: "apellido"},
                      { mData: "usuario"},
                      { mData: "email", bVisible: false, bSearchable: false},
                      { mData: "rut", bVisible: false, bSearchable: false},
                      { mData: "perfil", bSearchable: false, mRender: function(data, type, full){
                          // validacion referencia null
                          if(full.perfil != null){
                            return '<p>' + full.perfil.nombre + '</p>'
                          } else {
                            return '<p>Sin perfil</p>'
                          }
                        }
                      },
                      { mData: "empresa", bSearchable: false, mRender: function(data, type, full){
                          // validacion de referencia null
                          if(full.empresa != null){
                            return '<p>' + full.empresa.razonSocial + '</p>'
                          } else {
                            return '<p>Sin empresa</p>'
                          }
                        }
                      },
                      { mData: null ,
                        mRender: function(data, type, full){
                          return '<a href="#" data-toggle="modal" data-target="#updModal" class="edit" >actualizar</a>'
                          //return '<a href="/updpermiso/' + full._id + '">actualizar</a>'
                        }
                      },
                      { mData: null,
                        mRender: function(data, type, full){
                          return '<a href="/delejecutivo/' + full._id + '">eliminar</a>'
                        }
                      }
                    ],
                    fnDrawCallback: function( oSettings ) {
                      $('.dataTables_info').addClass("control-label");
                      $('.dataTables_filter input').attr('placeholder','Buscar');
                    },
                    fnServerParams: function(aoData) {
                      aoData.push({
                        name: "bChunkSearch", value: true
                      }); }
                  });

    $('#dtEjecutivos').on('click', '.edit' ,function(e){
        // datos de la fila
        var fila = e.currentTarget.parentElement.parentElement.cells[0];
        // posicion de la fila
        var aPos = dPertfiles.fnGetPosition( fila );
        // datos de la fila
        var aData = dPertfiles.fnGetData( aPos[0] );

        debugger;

        // nombre ejecutivo
        var nombre = aData.nombre;
        // apellido ejecutivo
        var apellido = aData.apellido;
        // rut ejecutivo
        var rut = aData.rut;
        // email ejecutivo
        var email = aData.email;
        // id ejecutivo
        var id = aData._id;

        $('#updNombre').val(nombre);
        $('#updApellido').val(apellido);
        $('#rut').val(rut);
        $('#updEmail').val(email);
        $('#idEjecutivo').val(id);
    });

  });
