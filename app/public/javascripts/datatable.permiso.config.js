$(document).ready(function() {

      var oTable = $('#dtPermisos').dataTable({
                    bFilter: true,
                    bLengthChange: false,
                    bProcessing: true,
                    bServerSide: true,
                    sAjaxSource: "/getpermisos",
                    iDisplayLength: 6,
                    oLanguage: {
                        "sLengthMenu": "Mostrando _MENU_ datos por pagina",
                        "sZeroRecords": "Nada encontrado",
                        "sInfo": "Monstrando _START_ de _END_ de _TOTAL_ datos",
                        "sInfoFiltered": "(filtrado de _MAX_ datos totales)",
                        "sInfoEmpty": "Mostrando 0 de 0 de 0 datos",
                        "sSearch": "Buscar",
                        "oPaginate": {
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        }
                    },
                    aoColumns: [
                      { mData: "nombrePagina" , sClass: "text-center"},
                      { mData: 'ingreso', sDefaultContent: "", sClass: "text-center",
                        mRender: function(data, type, full) {
                            return (data === true) ? '<span class="glyphicon glyphicon-ok"></span>' : '<span class="glyphicon glyphicon-remove"></span>';
                        }
                      },
                      { mData: "urlPagina", sClass: "text-center"},
                      { mData: null ,
                        mRender: function(data,type,full){
                          return '<a href="#" data-toggle="modal" data-target="#updModal" class="edit" >actualizar</a>'
                        }
                      },
                      { mData: null,
                        mRender: function(data, type, full){
                          return '<a href="/delpermiso/' + full._id + '">eliminar</a>'
                        }
                      }
                    ],
                    fnServerParams: function(aoData) {
                      aoData.push({
                        name: "bChunkSearch", value: true
                      }); }
                  }).columnFilter(
                    /*{
                    aoColumns: [
                      { type: "text" },
                      { type: null }
                    ]
                  }*/);

    $('#dtPermisos').on('click', '.edit' ,function(e){
        // datos de la fila
        var fila = e.currentTarget.parentElement.parentElement.cells[0];
        // posicion de la fila
        var aPos = oTable.fnGetPosition( fila );
        // datos de la fila
        var aData = oTable.fnGetData( aPos[0] );

        // nombre pagina
        var nombrePagina = aData.nombrePagina;
        // ingreso
        var ingreso = aData.ingreso;
        // url pagina
        var urlPagina = aData.urlPagina;
        // id Permiso
        var idPermiso = aData._id;

        if(aData.ingreso == true){
          $('#updRadiosTrue').attr('checked',true);
        } else {
          $('#updRadiosFalse').attr('checked',true);
        }

        $('#updNombrePagina').val(nombrePagina);
        $('#updUrlPagina').val(urlPagina);
        $('#idPermiso').val(idPermiso);
    });

  });
