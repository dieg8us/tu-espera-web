$(document).ready(function() {

    var socket = new io.connect();

    socket.on('connect', function() {

      // rescatando nombre de sucursal
      var sucursal = $('#nombre_sucursal').val();
      // ingresando a sucursal del ejecutivo
      socket.emit('ejecutivo', sucursal);
      // consultando los clientes conectados a la sucursal
      socket.emit('sucursal', sucursal);

      socket.on('visorcli', function(eclients, nclient, ratencion, modulo, iduser, numVisorAtencion, rutVisorAtencion) {

          // limpiando
          $('#numero_atencion').text('');
          $('#rut').text('');
          $('#modulo').text('');

          // asignando valores
          rutVisorAtencion = rutVisorAtencion.substring(0, 7) + '***-*';
          $('#numero_atencion').append(numVisorAtencion);
          $('#rut').append(rutVisorAtencion);
          $('#modulo').append(modulo);
      });

    });
});
