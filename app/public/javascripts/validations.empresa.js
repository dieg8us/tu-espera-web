$("#form-new-empresa").validate({
    rules: {
        razonSocial: {
            minlength: 1,
            maxlength: 60,
            required: true
        },
        cmbRubro: {
          required: true
        }
    },
    messages: {
        razonSocial: {
          required: "Razón Social requerida",
          minlength: "Ingresa minimo 1 caracteres",
          maxlength: "Ingresa maximo 60 carateres"
        },
        cmbRubro: {
          required: "Región requerida"
        }
    },
    showErrors: function(errorMap, errorList) {

      $.each(this.successList, function(index, value) {
        return $(value).popover("hide");
      });

      return $.each(errorList, function(index, value) {
        var _popover;
        _popover = $(value.element).popover({
          trigger: "manual",
          placement: "left",
          content: value.message,
          template: "<div class=\"popover\"><div class=\"arrow\"></div><div class=\"popover-inner\"><div class=\"popover-content\"><p></p></div></div></div>"
        });
        _popover.data("bs.popover").options.content = value.message;
        return $(value.element).popover("show");
      });
    }
});
$("#form-upd-empresa").validate({
    rules: {
        razonSocial: {
            minlength: 1,
            maxlength: 60,
            required: true
        },
        cmbRubro: {
          required: true
        }
    },
    messages: {
        razonSocial: {
          required: "Razón Social requerida",
          minlength: "Ingresa minimo 1 caracteres",
          maxlength: "Ingresa maximo 60 carateres"
        },
        cmbRubro: {
          required: "Región requerida"
        }
    },
    showErrors: function(errorMap, errorList) {

      $.each(this.successList, function(index, value) {
        return $(value).popover("hide");
      });

      return $.each(errorList, function(index, value) {
        var _popover;
        _popover = $(value.element).popover({
          trigger: "manual",
          placement: "left",
          content: value.message,
          template: "<div class=\"popover\"><div class=\"arrow\"></div><div class=\"popover-inner\"><div class=\"popover-content\"><p></p></div></div></div>"
        });
        _popover.data("bs.popover").options.content = value.message;
        return $(value.element).popover("show");
      });
    }
});

$("#form-new-sucursal").validate({
    rules: {
        nombreSucursal: {
            minlength: 1,
            maxlength: 60,
            required: true
        },
        calleSucursal: {
            minlength: 1,
            maxlength: 60,
            required: true
        },
         nroCalleSucursal: {
            minlength: 1,
            maxlength: 60,
            required: true
        },
        cmbRegionSucursal: {
          required: true
        },
        cmbComunaSucursal: {
          required: true
        },
         telefonoSucursal: {
            minlength: 1,
            maxlength: 60,
            required: true
        },
        serviciosSucursal: {
          required: true
        },
        horariosSucursal: {
          required: true
        }

    },
    messages: {
        nombreSucursal: {
          required: "Nombre requerido",
          minlength: "Ingresa minimo 1 caracteres",
          maxlength: "Ingresa maximo 60 carateres"
        },
        calleSucursal: {
          required: "Calle requerida",
          minlength: "Ingresa minimo 1 carateres",
          maxlength: "Ingresa maximo 60 carateres"
        },
        nroCalleSucursal: {
          required: "Nro de Calle requerido",
          minlength: "Ingresa minimo 1 carateres",
          maxlength: "Ingresa maximo 60 carateres"
        },
        cmbRegionSucursal: {
          required: "Región requerida"
        },
        cmbComunaSucursal: {
          required: "Comuna requerida"
        },
         telefonoSucursal: {
          required: "Telefono requerido",
          minlength: "Ingresa minimo 1 carateres",
          maxlength: "Ingresa maximo 60 carateres"
        },
         serviciosSucursal: {
          required: "Servicio requerido"
        },
        horariosSucursal: {
          required: "Horario requerido"
        },
    },
    showErrors: function(errorMap, errorList) {

      $.each(this.successList, function(index, value) {
        return $(value).popover("hide");
      });

      return $.each(errorList, function(index, value) {
        var _popover;
        _popover = $(value.element).popover({
          trigger: "manual",
          placement: "left",
          content: value.message,
          template: "<div class=\"popover\"><div class=\"arrow\"></div><div class=\"popover-inner\"><div class=\"popover-content\"><p></p></div></div></div>"
        });
        _popover.data("bs.popover").options.content = value.message;
        return $(value.element).popover("show");
      });
    }
});


$("#form-new-servicio").validate({
     rules: {
       servicioNombre: {
            minlength: 1,
            maxlength: 60,
            required: true
        },
        servicioDescripcion: {
            minlength: 1,
            maxlength: 200,
            required: true
        },
        cmbSucursal: {
          required: true
        }
    },
    messages: {
        servicioNombre: {
          required: "Nombre requerido",
          minlength: "Ingresa minimo 1 caracteres",
          maxlength: "Ingresa maximo 60 carateres"
        },
         servicioDescripcion: {
          required: "Descripción requerida",
          minlength: "Ingresa minimo 1 caracteres",
          maxlength: "Ingresa maximo 60 carateres"
        },
        cmbSucursal: {
          required: "Sucursal requerida"
        }
    },
    showErrors: function(errorMap, errorList) {

      $.each(this.successList, function(index, value) {
        return $(value).popover("hide");
      });

      return $.each(errorList, function(index, value) {
        var _popover;
        _popover = $(value.element).popover({
          trigger: "manual",
          placement: "left",
          content: value.message,
          template: "<div class=\"popover\"><div class=\"arrow\"></div><div class=\"popover-inner\"><div class=\"popover-content\"><p></p></div></div></div>"
        });
        _popover.data("bs.popover").options.content = value.message;
        return $(value.element).popover("show");
      });
    }
});




$.validator.addMethod("rut", function(value, element) {
  return this.optional(element) || $.Rut.validar(value);
}, "Este campo debe ser un rut valido" );


$("#jq-validation").validate();

