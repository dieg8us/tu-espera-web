$("#form-perfil").validate({
    rules: {
        nom_perfil: {
            minlength: 4,
            maxlength: 10,
            required: true
        },
        descripcion: {
            minlength: 30,
            maxlength: 70,
            required: true
        }
    },
    messages: {
        nom_perfil: {
          required: "Nombre perfil requerido",
          minlength: "Ingresa minimo 4 caracteres",
          maxlength: "Ingresa maximo 10 carateres"
        },
        descripcion: {
          required: "Descripción del perfil requerida",
          minlength: "Ingresa minimo 30 carateres",
          maxlength: "Ingresa maximo 70 carateres"
        }
    },
    showErrors: function(errorMap, errorList) {

      $.each(this.successList, function(index, value) {
        return $(value).popover("hide");
      });

      return $.each(errorList, function(index, value) {
        var _popover;
        _popover = $(value.element).popover({
          trigger: "manual",
          placement: "top",
          content: value.message,
          template: "<div class=\"popover\"><div class=\"arrow\"></div><div class=\"popover-inner\"><div class=\"popover-content\"><p></p></div></div></div>"
        });

        // Bootstrap 3.x :
        _popover.data("bs.popover").options.content = value.message;
        // Bootstrap 2.x :
        //_popover.data("popover").options.content = value.message;
        return $(value.element).popover("show");
      });
    }
});

$("#form-upd-perfil").validate({
    rules: {
        updNombre: {
            minlength: 4,
            maxlength: 10,
            required: true
        },
        updDescripcion: {
            minlength: 30,
            maxlength: 70,
            required: true
        }
    },
    messages: {
        updNombre: {
          required: "Nombre perfil requerido",
          minlength: "Ingresa minimo 4 caracteres",
          maxlength: "Ingresa maximo 10 carateres"
        },
        updDescripcion: {
          required: "Descripción del perfil requerida",
          minlength: "Ingresa minimo 30 carateres",
          maxlength: "Ingresa maximo 70 carateres"
        }
    },
    showErrors: function(errorMap, errorList) {

      $.each(this.successList, function(index, value) {
        return $(value).popover("hide");
      });

      return $.each(errorList, function(index, value) {
        var _popover;
        _popover = $(value.element).popover({
          trigger: "manual",
          placement: "bottom",
          content: value.message,
          template: "<div class=\"popover\"><div class=\"arrow\"></div><div class=\"popover-inner\"><div class=\"popover-content\"><p></p></div></div></div>"
        });

        // Bootstrap 3.x :
        _popover.data("bs.popover").options.content = value.message;
        // Bootstrap 2.x :
        //_popover.data("popover").options.content = value.message;
        return $(value.element).popover("show");
      });
    }
});
