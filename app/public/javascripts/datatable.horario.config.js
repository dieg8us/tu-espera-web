$(document).ready(function() {

      var dPertfiles = $('#dtHorarios').dataTable({
                    bFilter: true,
                    bLengthChange: false,
                    bProcessing: true,
                    bServerSide: true,
                    bSort:       false,
                    sAjaxSource: "/gethorarios",
                    iDisplayLength: 4,
                    oLanguage: {
                        "sLengthMenu": "Mostrando _MENU_ datos por pagina",
                        "sZeroRecords": "Nada encontrado",
                        "sInfo": "Monstrando _START_ de _END_ de _TOTAL_ datos",
                        "sInfoFiltered": "(filtrado de _MAX_ datos totales)",
                        "sInfoEmpty": "Mostrando 0 de 0 de 0 datos",
                        "sSearch": "",
                        "oPaginate": {
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        }
                    },
                    aoColumns: [
                      { mData: "nombre"},
                      { mData: "mes",
                        mRender: function(data, type, full) {
                            // case de retorno
                            switch(data) {
                                case "1":
                                  return 'enero';
                                  break;
                                case "2":
                                  return 'febrero';
                                  break;
                                case "3":
                                  return 'marzo';
                                  break;
                                case "4":
                                  return 'abril';
                                  break;
                                case "5":
                                  return 'mayo';
                                  break;
                                case "6":
                                  return 'junio';
                                  break;
                                case "7":
                                  return 'julio';
                                  break;
                                case "8":
                                  return 'agosto';
                                  break;
                                case "9":
                                  return 'septiembre';
                                  break;
                                case "10":
                                  return 'octubre';
                                  break;
                                case "11":
                                  return 'nobiembre';
                                  break;
                                case "12":
                                  return 'diciembre';
                                  break;
                            }
                        }
                      },
                      { mData: "detalle", bVisible: false },
                      { mData: null ,
                        mRender: function(data, type, full){
                          return '<a href="#" data-toggle="modal" data-target="#updModal" class="edit" >actualizar</a>'
                          //return '<a href="/updpermiso/' + full._id + '">actualizar</a>'
                        }
                      },
                      { mData: null,
                        mRender: function(data, type, full){
                          return '<a href="/delhorario/' + full._id + '">eliminar</a>'
                        }
                      }
                    ],
                    fnDrawCallback: function( oSettings ) {
                      $('.dataTables_info').addClass("control-label");
                      $('.dataTables_filter input').attr('placeholder','Buscar');
                    },
                    fnServerParams: function(aoData) {
                      aoData.push({
                        name: "bChunkSearch", value: true
                      }); }
                  }).columnFilter(
                    /*{
                    aoColumns: [
                      { type: "text" },
                      { type: null }
                    ]
                  }*/);

    $('#dtHorarios').on('click', '.edit' ,function(e){
        // datos de la fila
        var fila = e.currentTarget.parentElement.parentElement.cells[0];
        // posicion de la fila
        var aPos = dPertfiles.fnGetPosition( fila );
        // datos de la fila
        var aData = dPertfiles.fnGetData( aPos[0] );

        // nombre Horario
        var nombreHorario = aData.nombre;
        // Mes Horario
        var mesHorario = aData.mes;
        // id Horario
        var idHorario = aData._id;
        // hora inicio dia lunes Horario
        var horarioDetalle = aData.detalle;

        $('#updNombre').val(nombreHorario);
        $('#updMes').val(mesHorario);
        $('#idHorario').val(idHorario);

        // horario

        $('#updInicioLunes').val(horarioDetalle[0].inicioLunes)
        $('#updInicioMartes').val(horarioDetalle[0].inicioMartes)
        $('#updInicioMiercoles').val(horarioDetalle[0].inicioMiercoles)
        $('#updInicioJueves').val(horarioDetalle[0].inicioJueves)
        $('#updInicioViernes').val(horarioDetalle[0].inicioViernes)
        $('#updInicioSabado').val(horarioDetalle[0].inicioSabado)
        $('#updInicioDomingo').val(horarioDetalle[0].inicioDomingo)

        $('#updTerminoLunes').val(horarioDetalle[0].terminoLunes)
        $('#updTerminoMartes').val(horarioDetalle[0].terminoMartes)
        $('#updTerminoMiercoles').val(horarioDetalle[0].terminoMiercoles)
        $('#updTerminoJueves').val(horarioDetalle[0].terminoJueves)
        $('#updTerminoViernes').val(horarioDetalle[0].terminoViernes)
        $('#updTerminoSabado').val(horarioDetalle[0].terminoSabado)
        $('#updTerminoDomingo').val(horarioDetalle[0].terminoDomingo)
    });

  });
