$(document).ready(function() {

      var dPertfiles = $('#dtPerfiles').dataTable({
                    bFilter: true,
                    bLengthChange: false,
                    bProcessing: true,
                    bServerSide: true,
                    bSort:       false,
                    sAjaxSource: "/getperfiles",
                    iDisplayLength: 4,
                    oLanguage: {
                        "sLengthMenu": "Mostrando _MENU_ datos por pagina",
                        "sZeroRecords": "Nada encontrado",
                        "sInfo": "Monstrando _START_ de _END_ de _TOTAL_ datos",
                        "sInfoFiltered": "(filtrado de _MAX_ datos totales)",
                        "sInfoEmpty": "Mostrando 0 de 0 de 0 datos",
                        "sSearch": "",
                        "oPaginate": {
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        }
                    },
                    aoColumns: [
                      { mData: "nombre"},
                      { mData: "descripcion"},
                      { mData: null ,
                        mRender: function(data, type, full){
                          return '<a href="#" data-toggle="modal" data-target="#updModal" class="edit" >actualizar</a>'
                          //return '<a href="/updpermiso/' + full._id + '">actualizar</a>'
                        }
                      },
                      { mData: null,
                        mRender: function(data, type, full){
                          return '<a href="/delperfil/' + full._id + '">eliminar</a>'
                        }
                      }
                    ],
                    fnDrawCallback: function( oSettings ) {
                      $('.dataTables_info').addClass("control-label");
                      $('.dataTables_filter input').attr('placeholder','Buscar');
                    },
                    fnServerParams: function(aoData) {
                      aoData.push({
                        name: "bChunkSearch", value: true
                      }); }
                  }).columnFilter(
                    /*{
                    aoColumns: [
                      { type: "text" },
                      { type: null }
                    ]
                  }*/);

    $('#dtPerfiles').on('click', '.edit' ,function(e){
        // datos de la fila
        var fila = e.currentTarget.parentElement.parentElement.cells[0];
        // posicion de la fila
        var aPos = dPertfiles.fnGetPosition( fila );
        // datos de la fila
        var aData = dPertfiles.fnGetData( aPos[0] );

        // nombre pagina
        var nombrePerfil = aData.nombre;
        // url pagina
        var descripcionPerfil = aData.descripcion;
        // id Permiso
        var idPerfil = aData._id;

        $('#updNombre').val(nombrePerfil);
        $('#updDescripcion').val(descripcionPerfil);
        $('#idPerfil').val(idPerfil);
    });

  });
