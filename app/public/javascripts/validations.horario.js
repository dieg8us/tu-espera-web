$("#form-horario").validate({
    rules: {
        nom_horario:{
          required: true
        },
        mes:{
          required: true
        }
        // inicio_lunes: {
        //   required: true
        // },
        // inicio_martes: {
        //     required: true
        // },
        // inicio_miercoles: {
        //     required: true
        // },
        // inicio_jueves:{
        //     required: true
        // },
        // inicio_viernes: {
        //     required: true
        // },
        // inicio_sabado: {
        //   required: true
        // },
        // inicio_domingo: {
        //   required: true
        // },
        // termino_lunes: {
        //   required: true
        // },
        // termino_martes: {
        //   required:true
        // },
        // termino_miercoles: {
        //   required: true
        // },
        // termino_jueves: {
        //   required: true
        // },
        // termino_viernes: {
        //   required: true
        // },
        // termino_sabado: {
        //   required: true
        // },
        // termino_domingo: {
        //   required: true
        // }
    },
    messages: {
        nom_horario:{
          required: "Nombre de horario requerido"
        },
        mes: {
          required: "Seleccione mes del horario"
        }
        // inicio_lunes: {
        //   required: "Hora requerida"
        // },
        // inicio_martes: {
        //   required: "Hora requerida"
        // },
        // inicio_miercoles: {
        //   required: "Hora requerida"
        // },
        // inicio_jueves: {
        //   required: "Hora requerida"
        // },
        // inicio_viernes:{
        //   required: "Hora requerida"
        // },
        // inicio_sabado: {
        //   required: "Hora requerida"
        // },
        // inicio_domingo: {
        //   required: "Hora requerida"
        // },
        // termino_lunes: {
        //   required: "Hora requerida"
        // },
        // termino_martes: {
        //   required: "Hora requerida"
        // },
        // termino_miercoles: {
        //   required: "Hora requerida"
        // },
        // termino_jueves: {
        //   required: "Hora requerida"
        // },
        // termino_viernes: {
        //   required: "Hora requerida"
        // },
        // termino_sabado: {
        //   required: "Hora requerida"
        // },
        // termino_domingo: {
        //   required: "Hora requerida"
        // }
    },
    showErrors: function(errorMap, errorList) {

      $.each(this.successList, function(index, value) {
        return $(value).popover("hide");
      });

      return $.each(errorList, function(index, value) {
        var _popover;
        _popover = $(value.element).popover({
          trigger: "manual",
          placement: "top",
          content: value.message,
          template: "<div class=\"popover\"><div class=\"arrow\"></div><div class=\"popover-inner\"><div class=\"popover-content\"><p></p></div></div></div>"
        });

        // Bootstrap 3.x :
        _popover.data("bs.popover").options.content = value.message;
        // Bootstrap 2.x :
        //_popover.data("popover").options.content = value.message;
        return $(value.element).popover("show");
      });
    }
});

$.validator.addMethod("time", function(value, element) {
return this.optional(element) || /^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])(:([0-5]?[0-9]))?$/i.test(value);
}, "Ingresar una hora valida.");

// $.validator.addMethod("endate_greater_startdate", function(value, element) {
//     return enddate > startdate
// }, "* Enddate should be greater than Startdate");

$('.clockpicker').clockpicker({
    placement: 'top',
    align: 'left',
    autoclose: true,
    'default': 'now'
});
