$(document).ready(function() {
  var dAtenciones = $('#dtAtenciones').dataTable({
    bFilter: true,
    bLengthChange: false,
    bProcessing: true,
    bServerSide: true,
    bSort:       false,
    sAjaxSource: "/getatenciones",
    iDisplayLength: 4,
    oLanguage: {
      "sLengthMenu": "Mostrando _MENU_ datos por pagina",
      "sZeroRecords": "Nada encontrado",
      "sInfo": "Monstrando _START_ de _END_ de _TOTAL_ datos",
      "sInfoFiltered": "(filtrado de _MAX_ datos totales)",
      "sInfoEmpty": "Mostrando 0 de 0 de 0 datos",
      "sSearch": "",
      "oPaginate": {
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
      }
    },
    aoColumns: [
    { mData: "sucursal"},
    { mData: "rutPersona"},
    { mData: "estadoAtencion", mRender: function(data, type, full) {
          if(full.estadoAtencion != null){

            switch(full.estadoAtencion) {
              case 'atendido':
                return '<p>Atendido</p>'
                break;
              case 'noatendido':
                return '<p>No Atendido</p>'
                break;
            }

            // return '<p>' + full.estadoAtencion + '</p>'
          } else {
            return '<p>Sin Atencion</p>'
          }
      }
    },
    { mData: "comentario"},
    { mData: "fecha", mRender: function(data, type, full) {
        // return full.fecha.format('mm/dd/YYYY');
        var value = new Date(full.fecha.substr(0,10));
        var ret = value.toLocaleDateString();
        return '<p>'+ ret +'</p>'
      }
    },
    { mData: "numeroAtencion"},
    { mData: "inicioEspera", mRender: function(data, type, full) {
        // return full.fecha.format('mm/dd/YYYY');
        var value = new Date(full.inicioEspera);
        var ret = value.getTime();
        return '<p>'+ value.getHours() + ':' + value.getMinutes() +'</p>'
      }
    },
    { mData: "terminoEspera", mRender: function(data, type, full) {
        // return full.fecha.format('mm/dd/YYYY');
        var value = new Date(full.terminoEspera);
        var ret = value.getTime();
        return '<p>'+ value.getHours() + ':' + value.getMinutes() +'</p>'
      }
    }
    ],
    fnDrawCallback: function( oSettings ) {
      $('.dataTables_info').addClass("control-label");
      $('.dataTables_filter input').attr('placeholder','Buscar');
    },
    fnServerParams: function(aoData) {
      aoData.push({
        name: "bChunkSearch", value: true
      }); }
    }).columnFilter(
      /*{
      aoColumns: [
      { type: "text" },
      { type: null }
      ]
    }*/);

    function exportTableToCSV($table, filename) {

        var $rows = $table.find('tr:has(td)'),

            // Temporary delimiter characters unlikely to be typed by keyboard
            // This is to avoid accidentally splitting the actual contents
            tmpColDelim = String.fromCharCode(11), // vertical tab character
            tmpRowDelim = String.fromCharCode(0), // null character

            // actual delimiter characters for CSV format
            colDelim = '","',
            rowDelim = '"\r\n"',

            // Grab text from table into CSV formatted string
            csv = '"' + $rows.map(function (i, row) {
                var $row = $(row),
                    $cols = $row.find('td');

                return $cols.map(function (j, col) {
                    var $col = $(col),
                        text = $col.text();

                    return text.replace(/"/g, '""'); // escape double quotes

                }).get().join(tmpColDelim);

            }).get().join(tmpRowDelim)
                .split(tmpRowDelim).join(rowDelim)
                .split(tmpColDelim).join(colDelim) + '"',

            // Data URI
            csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

        $(this)
            .attr({
            'download': filename,
                'href': csvData,
                'target': '_blank'
        });
    }

    // This must be a hyperlink
    $("#export").on('click', function (event) {
        // CSV
        exportTableToCSV.apply(this, [$('#dtAtenciones'), 'atenciones.csv']);
    });
});
