$(document).ready(function() {

      var dEmpresas= $('#dtEmpresas').dataTable({
                    bFilter: true,
                    bLengthChange: false,
                    bProcessing: true,
                    bServerSide: true,
                    bSort:       false,
                    sAjaxSource: "/getempresas",
                    iDisplayLength:8,
                    oLanguage: {
                        "sLengthMenu": "Mostrando _MENU_ datos por pagina",
                        "sZeroRecords": "Nada encontrado",
                        "sInfo": "Monstrando _START_ de _END_ de _TOTAL_ datos",
                        "sInfoFiltered": "(filtrado de _MAX_ datos totales)",
                        "sInfoEmpty": "Mostrando 0 de 0 de 0 datos",
                        "sSearch": "",
                        "oPaginate": {
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        }
                    },
                    aoColumns: [
                      { mData: "razonSocial"},
                      { mData: "rut"},
                      { mData: "rubro", bSearchable: true, mRender: function(data, type, empresa){
                          // validacion referencia null
                          if(empresa.rubro.nombre != null){
                            return '<p>' + empresa.rubro.nombre + '</p>'
                          } else {
                            return 'Sin rubro'
                          }
                        }
                      },
                      { mData: "telefono"},
                      { mData: "fax"},
                      { mData: "flagEliminado", bVisible: false},
                      { mData: null ,
                        mRender: function(data, type, full){
                           return '<a href="/empresas/' + full._id + '"><span class="glyphicon glyphicon-edit"></a>'
                        }
                      },
                      { mData: null,
                        mRender: function(data, type, full){
                          return '<a href="/eliminarEmpresa/' + full._id + '"><span class="glyphicon glyphicon-minus-sign"></a>'
                        }
                      }
                    ],
                    fnDrawCallback: function( oSettings ) {
                      $('.dataTables_info').addClass("control-label");
                      $('.dataTables_filter input').attr('placeholder','Buscar');
                    },
                    fnServerParams: function(aoData) {
                      aoData.push({
                        name: "bChunkSearch", value: true
                      }); }
                  }).columnFilter();
  });
