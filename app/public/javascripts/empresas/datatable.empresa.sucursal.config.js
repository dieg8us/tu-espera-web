$(document).ready(function() {

      var idEmpresa = $('#id_empresa').val();

      var dtSucursales = $('#dtSucursales').dataTable({
                    bFilter: true,
                    bLengthChange: false,
                    bProcessing: true,
                    bServerSide: true,
                    bSort:       false,
                    sAjaxSource: "/empresas/getSucursales/" + idEmpresa,
                    iDisplayLength: 7,
                    oLanguage: {
                        "sLengthMenu": "Mostrando _MENU_ datos por pagina",
                        "sZeroRecords": "Nada encontrado",
                        "sInfo": "Monstrando _START_ de _END_ de _TOTAL_ datos",
                        "sInfoFiltered": "(filtrado de _MAX_ datos totales)",
                        "sInfoEmpty": "Mostrando 0 de 0 de 0 datos",
                        "sSearch": "",
                        "oPaginate": {
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        }
                    },
                    aoColumns: [
                      { mData: "nombre"},
                      { mData: "direccion", bSearchable: true, mRender: function(data, type, sucursal){

                          // validacion referencia null
                          if(sucursal.direccion != null){
                            return '<p>' + sucursal.direccion[0].calle + '</p>'
                          } else {
                            return 'Sin calle'
                          }
                        }
                      },
                      { mData: "direccion", bSearchable: true, mRender: function(data, type, sucursal){
                          // validacion referencia null
                          if(sucursal.direccion != null){
                            return '<p>' + sucursal.direccion[0].numCalle + '</p>'
                          } else {
                            return 'Sin nro de calle'
                          }
                        }
                      },
                      // { mData: "direccion", bSearchable: true, mRender: function(data, type, sucursal) {
                      //     // validacion referencia null
                      //     if(sucursal.direccion[0] != null){
                      //       return '<p>' + sucursal.direccion[0].comuna + '</p>'
                      //     } else {
                      //       return 'Sin región'
                      //     }
                      //   }
                      // },
                      // { mData: "direccion", bSearchable: true, mRender: function(data, type, sucursal){
                      //     // validacion referencia null
                      //     if(sucursal.direccion[0] != null){
                      //         return '<p>' + sucursal.direccion[0].comuna + '</p>'
                      //     } else {
                      //       return 'Sin comuna'
                      //     }
                      //   }
                      // },
                      { mData: "telefono"},
                      { mData: null,
                        mRender: function(data, type, full){
                          return '<a href="#" data-toggle="modal" data-target="#updateSucursalModal" class="edit" ><span class="glyphicon glyphicon-edit"></a>'
                        }
                      },
                      { mData: null,
                        mRender: function(data, type, full){
                          return '<a href="/empresas/eliminarSucursal/'+ idEmpresa +';' + full._id + '"><span class="glyphicon glyphicon-minus-sign"></a>'
                        }
                      },
                      { mData: null,
                        mRender: function(data, type, full) {
                          return '<a href="#" data-toggle="modal" data-target="#insCajaModal" class="edit" data-idsucursal="'+ full._id +'" data-nombresucursal="'+ full.nombre +'"><span class="glyphicon glyphicon-plus-sign"></span></a>'
                        }
                      },
                      { mData: null,
                        mRender: function(data, type, full) {
                          return '<a href="#" data-toggle="modal" data-target="#selCajaModal" class="edit" onclick="return loadCajas("'+ full._id +'")"><span class="glyphicon glyphicon-list-alt"></a>'
                        }
                      }
                    ],
                    fnDrawCallback: function( oSettings ) {
                      $('.dataTables_info').addClass("control-label");
                      $('.dataTables_filter input').attr('placeholder','Buscar');
                    },
                    fnServerParams: function(aoData) {
                      aoData.push({
                        name: "bChunkSearch", value: true
                      }); }
                   });
                  // }).columnFilter();




    $('#dtSucursales').on('click', '.edit' ,function(e){


        // datos de la fila
        var fila = e.currentTarget.parentElement.parentElement.cells[0];
        // posicion de la fila
        var aPos = dtSucursales.fnGetPosition( fila );
        // datos de la fila
        var aData = dtSucursales.fnGetData( aPos[0] );

       // nombre
        var nombreSucursal = aData.nombre;

        var direccion =  aData.direccion;

        var telefono = aData.telefono;

        var fax = aData.fax;

        var idSurcursal = aData._id;

        if(direccion!=null){

         var idComuna = direccion.comuna != null ? direccion.comuna._id : 0;

         var idRegion = direccion.comuna != null ? (direccion.comuna.region != null ? direccion.comuna.region._id: 0): 0;

         var calle = direccion.calle;

         var nroCalle = direccion.numCalle;
          // descripcion
        var latitud = direccion.latitud;

        var longitud = direccion.longitud;


          $('#nombreSucursal').val(nombreSucursal);
          //Seleccionamos el combo
          var selectRegion = $('#cmbRegionSucursal');
          selectRegion.val(idRegion);

           //Seleccionamos el combo
          var selectComuna = $('#cmbComunaSucursal');
          selectComuna.val(idComuna);

          $('#calleSucursal').val(calle);

          $('#nroCalleSucursal').val(nroCalle);

          $('#telefonoSucursal').val(telefono);

          $('#faxSucursal').val(fax);

      }

    });

  });
