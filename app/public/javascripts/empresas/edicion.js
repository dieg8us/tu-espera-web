  $(document).ready(function() {

      /* variable de id de empresa */
      var idEmpresa = $('#id_empresa').val();
      /* variable de id de rubro */
      var hddIdRubro =$('#hddIdRubro');

      /* multiselect control servicio de sucursal */
      $('#serviciosSucursal').multiselect();
      /* multiselect control horario sucursal */
      /* $('#horariosSucursal').multiselect(); */

      /* seteo de valor de id empresa sucursal */
      $('#idEmpresaSucursal').val(idEmpresa);
      /* seteo de valor de id empresa sucursal */
      $('#idEmpresaServicio_Nueva').val(idEmpresa);
      /* seteo de valor de id empresa sucursal */
      $('#idEmpresaServicio_Actualizar').val(idEmpresa);
      // cargar mapa con modal bootstrap 3
      $("#nuevaSucursalModal").on("shown.bs.modal", function () {
          google.maps.event.trigger(mapa, "resize");
      });
      /* se inicializa mapa cuando el boton de agregar sucursal se presiona */
      $('#AgregarSucursal').click(function(){
          InicializarMapa();
      });
      /* ubicacion de calle de sucursal en mapa */
      $('#nroCalleSucursal').blur(function() {
        ubicacion = $('#calleSucursal').val() + ' ' + $('#nroCalleSucursal').val() + ', ' + $('#cmbComunaSucursal option:selected').text() + ', Chile';
        //alert(ubicacion);
        GeoUbicar();
      });
      /* funcion de seleccion de comunas por sucursal */
      $("#cmbRegionSucursal").change(function() {
          /* id de region */
          var hddIdRegionSucursal = $('#hddIdRegionSucursal');
          var item = $(this);
          var idRegion = item.val();
          hddIdRegionSucursal.val(idRegion);

          var params = new Object();

          params.idRegion = idRegion;
          params = JSON.stringify(params);

          $.ajax({
              type: "POST",
              url: "/empresas/ObtenerComunasPorIdRegion",
              data: params,
              contentType: "application/json; charset=utf-8",
              dataType: "json",
              async: false,
              success: LoadComunas,
              error: function(XMLHttpRequest, textStatus, errorThrown) {
                  alert('ERROOOOR: '+textStatus + ": " + XMLHttpRequest.responseText);
              }
          });

      });

      /* Update Sucursal */

      /* funcion de seleccion de comunas por sucursal update */
      $("#UpdRegionSucursal").change(function() {
          /* id de region */
          var hddIdRegionSucursal = $('#UpdhddIdRegionSucursal');
          var item = $(this);
          var idRegion = item.val();
          hddIdRegionSucursal.val(idRegion);

          var params = new Object();

          params.idRegion = idRegion;
          params = JSON.stringify(params);

          $.ajax({
              type: "POST",
              url: "/empresas/ObtenerComunasPorIdRegion",
              data: params,
              contentType: "application/json; charset=utf-8",
              dataType: "json",
              async: false,
              success: LoadComunas,
              error: function(XMLHttpRequest, textStatus, errorThrown) {
                  alert('ERROOOOR: '+textStatus + ": " + XMLHttpRequest.responseText);
              }
          });

      });

      

      /* selecciona valor de rubros */
      SeleccionarValorCombo('cmbRubro',hddIdRubro.val());

      /* Ingreso de caja de Sucursal */

      // instancia cuando se muestra el modal de ingreso
      // de nueva caja
      $('#insCajaModal').on('show.bs.modal', function(event) {

          // rescatar enlace con idsucursal
          var link = $(event.relatedTarget); // link que ejecuto el levantamiento del modal-popup
          var idsucursal = link.data('idsucursal');
          var nombresucursal = link.data('nombresucursal');

          // seleccionar valor sucursal indicada en el row o fila

          $('#nombresucursal').text(nombresucursal);
          $('#idcajasucursal').val(idsucursal);
      });
  });

  /* variables */
  var mapa;
  var ubicacion = '';
  var lngDefault = -70.613927;
  var latDefault = -33.425079;
  var lng = -70.613927;
  var lat = -33.425079;
  var marker = null;
  var geocoder;
  var bounds;

  /* inicializacion de mapa */
  function InicializarMapa() {

     geocoder = new google.maps.Geocoder();
     bounds = new google.maps.LatLngBounds();

     var punto=new google.maps.LatLng(-33.433335, -70.614871);
        var opcionesMapa = {
          zoom: 16,
          center: punto
        };

        mapa = new google.maps.Map(document.getElementById('mapa'),opcionesMapa);

        var pointer = new google.maps.Marker({
          position: punto,
          icon: '',
          title: '',
          draggable: true,
          map: mapa,
      });
  }

  /* Geoubicacion */
  function GeoUbicar() {
     geocoder.geocode({ 'address': ubicacion }, function(results, status) {
         if (status == google.maps.GeocoderStatus.OK) {
             if (marker != null) marker.setMap(null);
             var posicion = results[0].geometry.location;
             lat = posicion.lat();
             lng = posicion.lng();
         }
          Desplegar();
     });
  }

  /* desplegar mapa */
  function Desplegar() {
      var posicion = new google.maps.LatLng(lat, lng);
      marker = new google.maps.Marker({
          position: posicion,
          map: mapa,
          title: ubicacion,
          animation: google.maps.Animation.DROP,
          draggable: true//Permite mover el icono
      });

      mapa.panTo(marker.getPosition());

      google.maps.event.addListenerOnce(mapa, 'bounds_changed', function(){
          bounds.extend(posicion);
          mapa.fitBounds(bounds);

          mapa.setCenter(posicion);
          SetearLatLngDesdeMapa(marker);

          mapa.setZoom(17);
      });

      google.maps.event.addListener(marker, 'dragend', function () {
          var punto = marker.getPosition();
          SetearLatLngDesdeMapa(marker);
          mapa.panTo(punto);
      });
  }

  /* seteo de latitud y longitud desde mapa */
  function SetearLatLngDesdeMapa(marca) {
    var pos = marca.getPosition();
    $('#latitud').val(pos.lat());
    $('#longitud').val(pos.lng());
    // alert('longitud : '+ $('#longitud').val() + ' latitud :'+$('#latitud').val());
  }

  function SetIdRubro(id) {
       var hddIdRubro =$('#hddIdRubro');
       hddIdRubro.val(id);
  }

  function SetIdSucursal(id) {
     var hddIdSucursalServicio =$('#hddIdSucursalServicio');
     hddIdSucursalServicio.val(id);
  }

  function SetIdRegionSucursal(id) {
     var hddIdRegionSucursal =$('#hddIdRegionSucursal');
     hddIdRegionSucursal.val(id);
  }

  function SetIdComunaSucursal(id) {
     var hddIdComunaSucursal =$('#hddIdComunaSucursal');
     hddIdComunaSucursal.val(id);
  }

  /* carga de comunas */
  function LoadComunas(result) {
    //quito los options que pudiera tener previamente el combo
    // cantidad de registros
    var cantidadRegistros = result.length;

    if(cantidadRegistros > 0) {

    var cmbComunaSucursal = $("#cmbComunaSucursal");
    cmbComunaSucursal.html("");
    cmbComunaSucursal.append($("<option></option>").attr("value",0).text('Seleccione'));

     for(var i = 0;i <cantidadRegistros;i++){
            var item = result[i];
            cmbComunaSucursal.append($("<option></option>").attr("value", item._id).text(item.nombre));
        }
      }
  }

  /* seleccina valor de combobox */
  function SeleccionarValorCombo( idCombo, idSeleccionar) {
      var select = document.getElementById(idCombo);

      for(var i = 0; i < select.options.length; i++) {
          var item = select.options[i];
        //alert('item id : '+item.value+' idSeleccionar :'+idSeleccionar);
        if(item.value == idSeleccionar ) {
          item.selected = true;
        }
      }
  }
