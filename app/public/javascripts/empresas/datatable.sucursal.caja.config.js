function loadCajas(idsucursal) {

    var sucursal = idsucursal;

    var dtSucursales = $('#dtCajas').dataTable({
          bFilter: true,
          bLengthChange: false,
          bProcessing: true,
          bServerSide: true,
          bSort:       false,
          sAjaxSource: '/empresas/sucursal/getcajas/' + sucursal.toString(),
          //aData: cajas,
          iDisplayLength: 7,
          oLanguage: {
              "sLengthMenu": "Mostrando _MENU_ datos por pagina",
              "sZeroRecords": "Nada encontrado",
              "sInfo": "Monstrando _START_ de _END_ de _TOTAL_ datos",
              "sInfoFiltered": "(filtrado de _MAX_ datos totales)",
              "sInfoEmpty": "Mostrando 0 de 0 de 0 datos",
              "sSearch": "",
              "oPaginate": {
                  "sNext": "Siguiente",
                  "sPrevious": "Anterior"
              }
          },
          aoColumns: [
            { mData: "numero"},
            { mData: "sucursal", bSearchable: true, mRender: function(data, type, sucursal) {
                // nombre sucursal
                return '<p>' + sucursal.nombre + '</p>'
              }
            }
          ],
          fnDrawCallback: function( oSettings ) {
            $('.dataTables_info').addClass("control-label");
            $('.dataTables_filter input').attr('placeholder','Buscar');
          },
          fnServerParams: function(aoData) {
            aoData.push({
              name: "bChunkSearch", value: true
            }); }
         });
  };
