$(document).ready(function() {

      var idEmpresa = $('#id_empresa').val();

      var dtServicios = $('#dtServicios').dataTable({
                    bFilter: true,
                    bLengthChange: false,
                    bProcessing: true,
                    bServerSide: true,
                    bSort:       false,
                    sAjaxSource: "/empresas/getServicios/" + idEmpresa,
                    iDisplayLength: 5,
                    oLanguage: {
                        "sLengthMenu": "Mostrando _MENU_ datos por pagina",
                        "sZeroRecords": "Nada encontrado",
                        "sInfo": "Monstrando _START_ de _END_ de _TOTAL_ datos",
                        "sInfoFiltered": "(filtrado de _MAX_ datos totales)",
                        "sInfoEmpty": "Mostrando 0 de 0 de 0 datos",
                        "sSearch": "",
                        "oPaginate": {
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        }
                    },
                    aoColumns: [

                      { mData: "sucursal", bSearchable: true, mRender: function(data, type, servicio){
                          // validacion referencia null
                          if(servicio.sucursal != null){
                            return '<p>' + servicio.sucursal.nombre + '</p>'
                          } else {
                            return 'Sin sucursal'
                          }
                        }
                      },
                      { mData: "nombre"},
                      { mData: "descripcion", bVisible: false},
                      { mData: null,
                        mRender: function(data, type, full) {
                          return '<a href="#" data-toggle="modal" data-target="#updServicioModal" class="edit" >actualizar</a>'
                        }
                      },
                      { mData: null,
                        mRender: function(data, type, full){
                          return '<a href="/empresas/eliminarServicio/'+ idEmpresa +';' + full._id + '">eliminar</a>'
                        }
                      }
                    ],
                    fnDrawCallback: function( oSettings ) {
                      $('.dataTables_info').addClass("control-label");
                      $('.dataTables_filter input').attr('placeholder','Buscar');
                    },
                    fnServerParams: function(aoData) {
                      aoData.push({
                        name: "bChunkSearch", value: true
                      }); }
                   });
                  // }).columnFilter();




    $('#dtServicios').on('click', '.edit' ,function(e){

        // datos de la fila
        var fila = e.currentTarget.parentElement.parentElement.cells[0];
        // posicion de la fila
        var aPos = dtServicios.fnGetPosition( fila );
        // datos de la fila
        var aData = dtServicios.fnGetData( aPos[0] );

        // nombre rubro
        var nombreServicio = aData.nombre;
        // descripcion
        var descripcionServicio = aData.descripcion;

        // id Rubro
        var idServicio = aData._id;

        var idSeleccionar = aData.sucursal._id;

        $('#updServicioNombre').val(nombreServicio);
        $('#updServicioDescripcion').val(descripcionServicio);
        $('#updIdServicio').val(idServicio);

        //Seleccionamos el combo
        var select = $('#updCmbSucursal');
        select.val(idSeleccionar);
    });

  });
