$(document).ready(function() {

    var socket = new io.connect();

    socket.on('connect', function() {

      // rescatando nombre de sucursal
      var sucursal = $('#nombre_sucursal').val();
      // ingresando a sucursal del ejecutivo
      socket.emit('ejecutivo', sucursal);
      // consultando los clientes conectados a la sucursal
      socket.emit('sucursal', sucursal);

      // funcion de atender
      $("#atender").click(function() {

        var asucursal = $('#nombre_sucursal').val();
        var modulo = $('#modulo_ejecutivo').val();
        var iduser = $('#iduser').val();
        var natencion = $('#cli_transito').text();
        var rut = $('#rut_transito').text();

        socket.emit('atencioncli', rut, natencion, modulo, iduser, asucursal);

        $("#espera_atencion").val(1);
        // mostrando
        $('#atendercli').addClass('hidden');
        // boton finalizar atencion
        $('#finatencion').removeClass('hidden');
        $('#finatencion').addClass('show');
        // estado atencion
        $('#estadoatencion').removeClass('hidden');
        $('#estadoatencion').addClass('show');
        // comentario
        $('#comentarioatencion').removeClass('hidden');
        $('#comentarioatencion').addClass('show');

        // socket.emit('sucursal', sucursal);
      });

      $("#siguiente").click(function() {

        var asucursal = $('#nombre_sucursal').val();
        var modulo = $('#modulo_ejecutivo').val();
        var iduser = $('#iduser').val();
        var natencion = $('#cli_transito').text();
        var rut = $('#rut_transito').text();
        var comentario = $('#comentario').val();
        var esAtencion = $('#estado').val();

        // actualizando datos atencion
        socket.emit('siguiente', iduser, rut, natencion, comentario, asucursal, esAtencion);

        // mostrando
        $('#atendercli').removeClass('hidden');
        $('#atendercli').addClass('show');
        // boton finalizar atencion
        $('#finatencion').removeClass('show');
        $('#finatencion').addClass('hidden');
        // estado atencion
        $('#estadoatencion').removeClass('show');
        $('#estadoatencion').addClass('hidden');
        // comentario
        $('#comentarioatencion').removeClass('show');
        $('#comentarioatencion').addClass('hidden');

      });

      socket.on('pass', function(user, sucursal) {
        // valores usuario y sucursal
        var iduser = $('#iduser').val();
        var asucursal = $('#nombre_sucursal').val();

        // validacion de sucursal y de usuario
        if(user == iduser) {
          if(asucursal == sucursal) {
            $("#espera_atencion").val(0);
            // consultando los clientes conectados a la sucursal
            socket.emit('sucursal', sucursal);
          }
        }

      });

      // refresh atencion
      socket.on('visorcli', function(eclients, nclient, ratencion, modulo, iduser, numVisorAtencion, rutVisorAtencion) {
        // valores flag
        var espera = $("#espera_atencion").val();
        var usuario = $('#iduser').val();

        // debugger;

        // si el usuario es distinto al usuario
        // que realizo la atencion
        if(usuario != iduser) {
          // se valida que el ejecutivo no se encuentre
          // atendiendo a una persona
          if(espera == 0) {
            // limpiando
            $('#cli_transito').text('');
            $('#rut_transito').text('');
            $('#cli_total').text('');

            // agregando nuevo valor
            $('#cli_transito').text(nclient);
            $('#rut_transito').text(ratencion);
            $('#cli_total').text(eclients);
          }
        }

      });

      // rescatando los clientes y mostrando en pagina
      socket.on('conn_cli', function(clientes, tespera, etransito, ratencion) {
          // total clientes conectados
          // limpiando
          $('#cli_total').text('');
          $('#cli_transito').text('');
          $('#rut_transito').text('');
          // agregando total
          if(clientes.conncli != null) {
            $('#cli_total').append(parseInt(clientes.conncli));
            $('#hcli_total').val(parseInt(clientes.conncli));
          } else {
            $('#cli_total').append('0');
            $('#hcli_total').val('0');
          }

          if(etransito.nclient != null) {
            $('#cli_transito').append(etransito.nclient);
            $('#hcli_transito').val(etransito.nclient);
          } else {
            $('#cli_transito').append('0');
            $('#hcli_transito').val('0');
          }

          if(ratencion.rut != null) {
            $('#rut_transito').append(ratencion.rut);
          } else {
            $('#rut_transito').append('0');
          }
          // agregando valores a campos hidden




          /* var minutos = tespera.time; */

          /*$('.countdown.styled').countdown({
            date: +(new Date) + minutos,
            render: function(data) {
              $(this.el).html("<div>" +*/
              /* this.leadingZeros(data.years, 4) + " <span>years</span></div><div>" + */
              /* this.leadingZeros(data.days, 3) + " <span>days</span></div><div>" + */
              /*this.leadingZeros(data.hours, 2) + " <span>hrs</span></div><div>" +
              this.leadingZeros(data.min, 2) + " <span>min</span></div><div>" +
              this.leadingZeros(data.sec, 2) + " <span>sec</span></div>");
            }
          });*/
      });

      socket.on('gen_ticket', function(natencion, rutpersona, cant) {
          // debugger;
          // validando rut transito
          var rut = $('#rut_transito').text();

          // asignando valor de rut transito
          if(rut == '0') {
            $('#rut_transito').text(rutpersona.rut);
          }
          // numero de atenciones
          var tAtenciones = cant.catenciones;
          // var tAtenciones = $('#cli_total').text();
          // se valida si el numero de atenciones
          // es mayor que 0
          // if(tAtenciones > 0) {
            // se suma uno al numero de atenciones
            // tAtenciones = parseInt(tAtenciones) + 1;
          // } else {
            // se agrega valor al numero de atenciones
            // tAtenciones = 1;
          // }

          // validacion cliente en transito
          var enTransito = $('#cli_transito').text();

          if(enTransito == '0'){
            // limpiando cliente en transito
            $('#cli_transito').text('');
            // agregando valor de ticket en transito
            $('#cli_transito').append(natencion.ticket);
          }

          // actualizando total clientes en fila
          // limpiando textos
          $('#cli_total').text('');
          // agregando total
          $('#cli_total').append(tAtenciones);

          var tespera = $('#cli_total').text();

          var minutos = parseInt(tespera) * 300000;
          var nDate = (new Date) + minutos;

          // $(".countdown").data('countdown').update(+(new Date) + minutos).start();

      });

    });
});
