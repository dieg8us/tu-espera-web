var Sucursal  = require("../models/SucursalModel"),
    Atencion  = require("../models/AtencionModel"),
    Ejecutivo = require("../models/EjecutivoModel");

var sockets = function(app, passport, io) {

  // sucursales en arreglo
  var aSucursales;

  Sucursal.find( { } , { nombre: 1, _id: 0}, function(err, sucursales) {
    aSucursales = sucursales;
  });

  // funcion que obtiene a todos los miembros conectados a
  // una habitacion de tickets
  function getAllRoomMembers(room, _nsp) {
    var roomMembers = [];
    var nsp = (typeof _nsp !== 'string') ? '/' : _nsp;

    for( var member in io.nsps[nsp].adapter.rooms[room] ) {
      roomMembers.push(member);
    }

    return roomMembers;
  }

  // abriendo puerto de socket.io para el servidor
  io.sockets.on('connection', function(socket) {

    // timpo de espera de los clientes
    var timpoEspera;

    // conexion de ejecutivo
    socket.on('ejecutivo', function(sucursal) {
      // uniendose a la sucursal de atencion
      socket.join(sucursal);
      console.log('ejecutivo ingreso a ' + sucursal);
    });

    // funcion que rescata todos los clientes conectados
    // a una sucursal en especial
    socket.on('sucursal', function(sucursal) {
      // buscar atenciones en cola
      Atencion.find({ estado: 0, sucursal: sucursal }, function(err, atenciones) {
         if(!err) {
            Atencion.find({ estado: 0 }).sort({ numeroAtencion: 1 }).limit(1).exec(function(err, natencion) {
                if(!err) {
                  // clientes en espera dentro de la sucursal
                  // debugger;
                  // validacion de atenciones dentro de sucursal
                  if(natencion.length > 0) {

                    var eclients;
                    var nclient;
                    var tespera;
                    var ratencion;

                    if(atenciones.length > 0){
                      eclients = atenciones.length;
                    } else {
                      eclients = 0;
                    }

                    if (natencion.length > 0) {
                      nclient = natencion[0].numeroAtencion;
                      ratencion = natencion[0].rutPersona;
                    } else {
                      nclient = 0;
                    }

                    if(eclients != 0) {
                      // tespera = 300000 * eclients;
                      tespera = 300000;
                    } else {
                      tespera = 0;
                    }

                    var ejecutivo = null;

                    if(natencion[0].ejecutivo) {
                      var fejecutivo = natencion[0].ejecutivo;
                    }

                    if(fejecutivo != null) {
                      // busqueda de ejecutivo
                      Ejecutivo.findById(fejecutivo, function(err, ejecutivo) {
                        // validacion de consulta
                        if(!err) {
                          // validacion de busqueda de ejecutivo
                          if(ejecutivo) {
                            // retorna datos de atencion
                            socket.emit('conn_cli',{ conncli: eclients }, { time: tespera }, { nclient: nclient }, { rut: ratencion }, { modulo: ejecutivo.modulo });
                          } else {
                            // retorna datos de atencion
                            socket.emit('conn_cli', { conncli: eclients }, { time: tespera }, { nclient: nclient }, { rut: ratencion }, { modulo: 0 });
                          }
                        } else {
                          // retorna datos de atencion
                          socket.emit('conn_cli', { conncli: eclients }, { time: tespera }, { nclient: nclient }, { rut: ratencion }, { modulo: 0 });
                        }
                      });
                    } else {
                      socket.emit('conn_cli', { conncli: eclients }, { time: tespera }, { nclient: nclient }, { rut: ratencion }, { modulo: 0 });
                    } // fejecutivo != null
                  } else {
                    // retorna datos de atencion
                    socket.emit('conn_cli', { conncli: eclients }, { time: tespera }, { nclient: nclient }, { rut: ratencion }, { modulo: 0 });
                  } // end if natencion.lenght

                } // end if err
            });
         }
      });
    });

    // atencion de cliente
    socket.on('atencioncli', function(rut, numerocli, modulo, iduser, sucursal) {

        var perRut = rut;
        var perNum = parseInt(numerocli);

        Atencion.findOne({ numeroAtencion: perNum, rutPersona: perRut }, function(err, atencion) {

            // validacion de consulta
            if(!err) {
              // validacion de atencion
              if(atencion != null) {
                  // actualizando estado

                  atencion.ejecutivo = iduser;
                  atencion.estado = 1;
                  atencion.esperaAtencion = 1;
                  atencion.inicioEspera = Date.now();
                  atencion.sucursal = sucursal;

                  // guardando estado
                  atencion.save(function(err, sAtencion) {
                      if(!err) {
                        // valores de atencion

                        var numVisorAtencion = sAtencion.numeroAtencion;
                        var rutVisorAtencion = sAtencion.rutPersona;

                        // atenciones
                        Atencion.find({ estado: 0, sucursal: sucursal }, function(err, atenciones) {
                            if(!err) {
                              // ultima atencion sin atender
                              Atencion.find({ estado: 0 }).sort({ numeroAtencion: 1 }).limit(1).exec(function(err, natencion) {
                                if(!err) {
                                  // se valida el ultimo numero de atencion
                                  if(natencion.length > 0) {

                                    var eclients;
                                    var nclient;
                                    var ratencion;

                                    if(atenciones.length > 0){
                                      eclients = atenciones.length;
                                    } else {
                                      eclients = 0;
                                    }

                                    if (natencion.length > 0) {
                                      nclient = natencion[0].numeroAtencion;
                                      ratencion = natencion[0].rutPersona;
                                    } else {
                                      nclient = 0;
                                    }

                                    if(eclients != 0) {
                                      // tespera = 300000 * eclients;
                                      tespera = 300000;
                                    } else {
                                      tespera = 0;
                                    }

                                    // retornando estado actual
                                    io.sockets.in(sucursal).emit('visorcli', eclients, nclient, ratencion, modulo, iduser, numVisorAtencion, rutVisorAtencion );
                                  } else {
                                    // retornando valores 0
                                    io.sockets.in(sucursal).emit('visorcli', 0, 0, 0, modulo, iduser, numVisorAtencion, rutVisorAtencion );
                                  }
                                } else {
                                  // retornando valores 0
                                  io.sockets.in(sucursal).emit('visorcli', 0, 0, 0, modulo, iduser, 0, 0);
                                }
                              });
                            } else {
                              // retornando valores 0
                              io.sockets.in(sucursal).emit('visorcli', 0, 0, 0, modulo, iduser, 0, 0);
                            }
                        });
                      }
                  }); // end save

              // end atencion null
              } else {
                io.sockets.in(sucursal).emit('visorcli', 0, 0, 0, modulo, iduser, 0, 0);
              }

              // end validacion err
            } else {
              io.sockets.in(sucursal).emit('visorcli', 0, 0, 0, modulo, iduser, 0, 0);
            }
        });
    });

    socket.on('siguiente', function(userid, rut, num, comentario, sucursal, esAtencion) {
        // buscar atencion y actualizar
        Atencion.findOne({ numeroAtencion: num, ejecutivo: userid, rutPersona: rut }, function(err, atencion) {
          if(!err){

            var iniEspera = atencion.inicioEspera;

            atencion.estadoAtencion = esAtencion;
            atencion.terminoEspera = Date.now();
            atencion.esperaAtencion = 0;
            atencion.comentario = comentario;
            atencion.promedioEspera = Date.now() - iniEspera;

            atencion.save(function(err, atencion) {
              if(!err) {
                socket.emit('pass', userid, sucursal);
              }
            });
          }
        });
    });

    // pruebas de conexion
    socket.on('init', function() {
      console.log('cel conectado a socket.io');
    });

    // contador de clientes conectados a
    // sucursal
    // socket.on('count', function(sucursal) {

    // total clientes conectados a sucursal
    // var roomclients = getAllRoomMembers(sucursal, '/');
    // retorna total de clientes conectados a la sucursal
    // socket.emit('cliespera', { message: roomclients.length });
    // });

    // funcion celular generacion de ticket
    socket.on('ticket', function(sucursal, rut_persona) {
      // uniendose a sucursal (generando ticket)
      socket.join(sucursal);
      // variable de ultima atencion
      var uAtencion = 0;

      // buscar el ultimo numero de atencion
      Atencion.find().sort({ numeroAtencion: -1 }).limit(1).exec(function(err, atenciones) {
        // debugger;
        if(!err) {
          // validacion de cantidad de atenciones
          if(atenciones.length > 0) {

            // ultima atencion
            uAtencion = atenciones[0].numeroAtencion;

            // fechas y horas
            var date = new Date();

            // numero de atencion
            var nAtencion = uAtencion + 1;

            var fecha = date;
            var hora = date.toLocaleTimeString();

            // nueva atencion
            var tAtencion = new Atencion();

            tAtencion.numeroAtencion = nAtencion;
            tAtencion.fecha = fecha;
            tAtencion.hora = hora;
            tAtencion.estado = 0;
            tAtencion.duracionAtencion = 5;
            tAtencion.sucursal = sucursal;
            tAtencion.rutPersona = rut_persona;

            // guardando solicitud de atencion
            tAtencion.save(function(err, atencion) {
              // validacion de consulta
              if(!err) {
                // numero de atencion
                nAtencion = atencion.numeroAtencion;
                // rut numero atencion
                rutAtencion = atencion.rutPersona;
                // enviando mensaje a todos
                Atencion.find({ estado: 0 }, function(err, cant) {
                    if(!err) {
                      io.sockets.in(sucursal).emit('gen_ticket',{ ticket: nAtencion }, { rut: rutAtencion }, { catenciones: cant.length });
                    }
                });
              }

            });

          } else {
            // ultima atencion
            uAtencion = 0;

            // fechas y horas
            var date = new Date();

            // numero de atencion
            var nAtencion = uAtencion + 1;

            var fecha = date;
            var hora = date.toLocaleTimeString();

            // nueva atencion
            var tAtencion = new Atencion();

            tAtencion.numeroAtencion = nAtencion;
            tAtencion.fecha = fecha;
            tAtencion.hora = hora;
            tAtencion.estado = 0;
            tAtencion.duracionAtencion = 5;
            tAtencion.sucursal = sucursal;
            tAtencion.rutPersona = rut_persona;

            // guardando solicitud de atencion
            tAtencion.save(function(err, atencion) {
              // validacion de error en la consulta
              if(!err) {
                // numero de atencion
                nAtencion = atencion.numeroAtencion;
                // rut numero atencion
                rutAtencion = atencion.rutPersona;
                // enviando mensaje a todos
                Atencion.find({ estado: 0 }, function(err, cant) {
                  if(!err) {
                    io.sockets.in(sucursal).emit('gen_ticket',{ ticket: nAtencion }, { rut: rutAtencion }, { catenciones: cant.length });
                  }
                });
              }

            });

          } // end else !err
        } // end if !err

      }); // end ultima atencion

    }); // end socket ticket

  });

}

module.exports = sockets;
