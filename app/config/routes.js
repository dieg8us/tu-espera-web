// import controllers
var indexController     = require("../controllers/IndexController"),
    homeController      = require("../controllers/HomeController"),
    rubroController     = require("../controllers/RubroController"),
    empresaController   = require("../controllers/EmpresaController"),
    ejecutivoController = require("../controllers/EjecutivoController"),
    perfilController    = require("../controllers/PerfilController"),
    permisoController   = require("../controllers/PermisoController"),
    horarioController   = require("../controllers/HorarioController"),
    sucursalController  = require("../controllers/SucursalController"),
    atencionController  = require("../controllers/AtencionController"),
    servicioController  = require("../controllers/ServicioController"),
    personaController   = require("../controllers/PersonaController"),
    cajaController      = require("../controllers/CajaController");



var routes = function(app, passport, io) {
  // se cargan los controladores a la aplicacion

  // index
  indexController(app, passport);
  // home
  homeController(app, passport);
  // rubro
  rubroController(app, passport);
  // ejecutivo
  ejecutivoController(app, passport);
  // perfil
  perfilController(app, passport);
  // permiso
  permisoController(app, passport);
  // empresa
  empresaController(app, passport);
  // horario
  horarioController(app, passport);
  // sucursal
  sucursalController(app, passport);
  // servicio
  servicioController(app, passport);
  // atencion
  atencionController(app, passport, io);
  // persona
  personaController(app, passport);
  //caja
  cajaController(app, passport);
};

module.exports = routes;
