//Librerias de mongoose
var mongoose = require("mongoose"),
    datatable = require("mongoose-datatable");

// configuracion de mongoose-datatable
datatable.configure({ debug: true, verbose: true });
mongoose.plugin(datatable.init);

//Nombre de la Base de datos
var dbname = "tuEspera";
//Conexion a la Base de datos Local
mongoose.connect('mongodb://127.0.0.1:27017/' + dbname);

console.log("cargando base de datos mongodb");

module.exports = mongoose;
