//Librerias
//passport local para login de aplicacion
var LocalStrategy = require("passport-local").Strategy;

//modelo ejecutivo para log-in
var Ejecutivo = require("../models/EjecutivoModel");

module.exports = function(passport) {
    console.log("cargando configuracion passport");

    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // requerido para sesiones persistentes

    // serializar el usuario
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // deserializar usuario
    passport.deserializeUser(function(id, done) {
        Ejecutivo.findById(id, function(err, user) {
            done(err, user);
        });
    });

    // =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================

    // passport.use('local-signup', new LocalStrategy({
    //     nombreField   : 'nombre',
    //     apellidoField : 'apellido',
    //     rutField      : 'rut',
    //     emailField    : 'email',
    //     usernameField : 'usuario',
    //     passwordField : 'password',
    //     passReqToCallback : true
    // },
    // function(req, username, password, nombre, apellido, rut, email, done) {
    //
    //     process.nextTick(function() {
    //
    //     //se verifica que no exista el usuario
    //     Ejecutivo.findOne({ 'usuario' :  username }, function(err, user) {
    //
    //         if (err)
    //             return done(err);
    //
    //         if (user) {
    //             return done(null, false, req.flash('signupMessage', 'El usuario ya se encuentra ingresado.'));
    //         } else {
    //
    //             var now = new Date.now();
    //
    //             //si no existe se crea el usuario
    //             var newUser = new Ejecutivo();
    //             //creadenciales
    //             newUser.nombre      = nombre;
    //             newUser.apellido    = apellido;
    //             newUser.rut         = rut;
    //             newUser.email       = email;
    //             newUser.fechaCreacion = now;
    //             newUser.flagEliminado = false;
    //             newUser.username    = username;
    //             newUser.contrasena  = newUser.generateHash(password);
    //
		// 		//guardando el usuario
    //             newUser.save(function(err) {
    //                 if (err)
    //                     throw err;
    //                 return done(null, newUser);
    //             });
    //         }
    //
    //     });
    //
    //     });
    //
    // }));

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================

    passport.use('local-login', new LocalStrategy({
          usernameField : 'username',
          passwordField : 'password',
          passReqToCallback : true
    },
    
    function(req, username, password, done) {

        //se verifica si el usuario existe en la base de datos
        Ejecutivo.findOne({ 'usuario' :  username }, function(err, user) {

            if (err)
                return done(err);

            //si no se encuentra el usuario
            if (!user)
                return done(null, false, req.flash('loginMessage', 'Usuario no existente.')); // req.flash is the way to set flashdata using connect-flash

			//contraseña erronea
            if (!user.validPassword(password))
                return done(null, false, req.flash('loginMessage', 'Oops, contraseña erronea.')); // create the loginMessage and save it to session as flashdata

            // se retorna el usuario de ingreso
            return done(null, user);
        });

    }));

};
