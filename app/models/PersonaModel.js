//Conexion a base de datos y libreria mongoose (modelos base de datos)
var models = require("../config/dbconnection"),
Schema = models.Schema,
relationship = require("mongoose-relationship"),
bcrypt   = require("bcrypt-nodejs");

//Objeto de modelo Mongoose
var PersonaSchema = new Schema({
  //propiedades
  nombre:             String,
  apellido:           String,
  rut:                String,
  email:              String,
  usuario:            String,
  password:           String,
  fechaCreacion:      Date,
  fechaModificacion:  Date,
  flagEliminado:      Boolean
});

//generando hash
// PersonaSchema.methods.generateHash = function(contrasena) {
//   return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
// };

//validando hash
// PersonaSchema.methods.validPassword = function(contrasena) {
//   return bcrypt.compareSync(password, this.contrasena);
// };

var Persona = models.model('Persona', PersonaSchema);

module.exports = Persona;
