//Conexion y libreria mongoose
var models = require("../config/dbconnection"),
    Schema = models.Schema;
    
    var rubroSchema = new Schema({
      nombre:    String,
      tipo:         String,
      fechaCreacion:      Date,
      fechaModificacion:  Date,
      flagEliminado:      Boolean,
      ejecutivo:          [{ type:Schema.ObjectId, ref:"Ejecutivo"}]
    });

var Rubro = models.model('Rubro', rubroSchema);

module.exports = Rubro;