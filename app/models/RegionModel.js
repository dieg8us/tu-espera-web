//Conexion y libreria mongoose
var models = require("../config/dbconnection"),
    Schema = models.Schema,
    relationship = require("mongoose-relationship");

    //objeto modelo mongoose
    var RegionSchema = new Schema({
        nombre:   String 
    });

var Region = models.model('Region', RegionSchema);

module.exports = Region;
