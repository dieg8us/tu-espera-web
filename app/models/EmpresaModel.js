//Conexion y libreria mongoose
var models = require("../config/dbconnection"),
    Schema = models.Schema;

    var empresaSchema = new Schema({
      rut:                String,
      razonSocial:        String,
      telefono:           String,
      fax:                String,
      fechaCreacion:      Date,
      fechaModificacion:  Date,
      flagEliminado:      Boolean,
      rubro:              [{ type:Schema.ObjectId, ref:"Rubro"}],
      ejecutivo:          [{ type:Schema.ObjectId, ref:"Ejecutivo"}],
      sucursal:           [{ type:Schema.ObjectId, ref:"Sucursal"}],
    });


var Empresa = models.model('Empresa', empresaSchema);

module.exports = Empresa;
