// Conexion a libreria mongoose
var models = require('../config/dbconnection'),
    Schema = models.Schema,
    relationship = require("mongoose-relationship");

// objeto modelo de horario
var DetalleHorarioSchema = new Schema({
  mes:      String,
  inicioLunes:        String,
  terminoLunes:       String,
  inicioMartes:       String,
  temrinoMartes:      String,
  inicioMiercoles:    String,
  terminoMiercoles:   String,
  inicioJueves:       String,
  terminoJueves:      String,
  inicioViernes:      String,
  terminoViernes:     String,
  inicioSabado:       String,
  terminoSabado:      String,
  inicioDomingo:      String,
  terminoDomingo:     String,
  fechaCreacion:      Date,
  // horario:            [{type:Schema.ObjectId, ref:"Horario"}],
  flagEliminado:      Boolean
});

var DetalleHorario = models.model('DetalleHorario', DetalleHorarioSchema);

module.exports = DetalleHorario;
