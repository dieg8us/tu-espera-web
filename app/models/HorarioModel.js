// Conexion a libreria mongoose
var models = require('../config/dbconnection'),
    Schema = models.Schema,
    relationship = require("mongoose-relationship");

// objeto modelo de horario
var HorarioSchema = new Schema({
  nombre:   String,
  mes:      String,
  fechaCreacion:   Date,
  usuario:  [{type:Schema.ObjectId, ref:"Ejecutivo"}],
  detalle:  [{type:Schema.ObjectId, ref:"DetalleHorario"}],
  flagEliminado:  Boolean
});

var Horario = models.model('Horario', HorarioSchema);

module.exports = Horario;
