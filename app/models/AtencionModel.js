//Conexion y libreria mongoose
var models = require("../config/dbconnection"),
    Schema = models.Schema,
    relationship = require("mongoose-relationship");

// objeto de modelo mongoogse
var AtencionShema = new Schema({
    numeroAtencion:   Number,
    siguienteNumero:  Number,
    fecha:            Date,
    hora:             String,
    duracionAtencion: Number,
    estado:           Number,
    estadoAtencion:   String,
    esperaAtencion:   Number,
    inicioEspera:     Date,
    terminoEspera:    Date,
    comentario:       String,
    sucursal:         String,
    rutPersona:       String,
    promedioEspera:   Number,
    ejecutivo:        { type:Schema.ObjectId, ref:"Ejecutivo" }
});

// relaciones de un servicio a muchas atenciones
//AtencionShema.plugin(relationship, {relationshipPathName: 'servicio'});

// modulo de modelo mongoose
var Atencion = models.model('Atencion', AtencionShema);

// exportar modulo
module.exports = Atencion;
