//Conexion y libreria mongoose
var models = require("../config/dbconnection"),
    Schema = models.Schema;
    
    //objeto modelo mongoose
    var estadoSchema = new Schema({
        nombre:             String,
        fecha:              Date,
        hora:               Date,
        fechaCreacion:      Date,
        fechaModificacion:  Date,
        flagEliminado:      Boolean,
        ejecutivo:          [{ type:Schema.ObjectId, ref:"Ejecutivo"}]
    });

var Estado = models.model('Estado', estadoSchema);

module.exports = Estado;