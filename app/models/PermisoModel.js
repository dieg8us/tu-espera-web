//Conexion y libreria mongoose
var models = require("../config/dbconnection"),
    Schema = models.Schema,
    relationship = require("mongoose-relationship");

    var permisoSchema = new Schema({
      nombrePagina:    String,
      ingreso:         Boolean,
      urlPagina:       String,
      flagEliminado:   Boolean,
      fechaCreacion:   Date,
      perfiles:        [{ type:Schema.ObjectId, ref:"Perfil", childPath:"permisos" }],
      ejecutivo:       [{ type:Schema.ObjectId, ref:"Ejecutivo" }]
    });

permisoSchema.plugin(relationship, { relationshipPathName:'perfiles' });

var Permiso = models.model('Permiso', permisoSchema);

module.exports = Permiso;
