//Conexion y libreria mongoose
var models = require("../config/dbconnection"),
    Schema = models.Schema,
    relationship = require("mongoose-relationship");

    //objeto modelo mongoose
    var DireccionSchema = new Schema({
        calle:              String,
        numCalle:           Number ,
        Geo:                { type: [Number], index: '2d' },
        longitud:           Number,
        latitud:            Number,
        flagEliminado:      Boolean,
        fechaCreacion:      Date,
        fechaModificacion:  Date,
        ejecutivo:          [{ type:Schema.ObjectId, ref:"Ejecutivo" }],
        sucursal:           [{ type:Schema.ObjectId, ref:"Sucursal" }],
        comuna:             [{ type:Schema.ObjectId, ref:"Comuna" }]
    });

//mapeo de la relacion a traves del plugin mongoose-relationship
//DireccionSchema.plugin(relationship, { relationshipPathName:'sucursal'});
//mapeo de la relacion a traves del plugin mongoose-relationship
//DireccionSchema.plugin(relationship, { relationshipPathName:'comuna'});


var Direccion = models.model('Direccion', DireccionSchema);

module.exports = Direccion;
