//Conexion y libreria mongoose
var models = require("../config/dbconnection"),
    Schema = models.Schema,
    relationship = require("mongoose-relationship");

// objeto de modelo mongoogse
var CajaShema = new Schema({
    numero:         Number,
    sucursal:       { type:Schema.ObjectId, ref:"Sucursal"},
    fechaIngreso:   Date,
    ejecutivo:      { type:Schema.ObjectId, ref:"Ejecutivo" }
});

// modulo de modelo mongoose
var Caja = models.model('Caja', CajaShema);

// exportar modulo
module.exports = Caja;
