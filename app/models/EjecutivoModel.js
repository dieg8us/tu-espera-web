//Conexion a base de datos y libreria mongoose (modelos base de datos)
var models = require("../config/dbconnection"),
    Schema = models.Schema,
    relationship = require("mongoose-relationship"),
    bcrypt   = require("bcrypt-nodejs");

  //Objeto de modelo Mongoose
  var EjecutivoSchema = new Schema({
    //propiedades
    nombre:             String,
    apellido:           String,
    rut:                String,
    email:              String,
    usuario:            String,
    contrasena:         String,
    fechaCreacion:      Date,
    fechaModificacion:  Date,
    flagEliminado:      Boolean,
    resetPasswordToken: String,
    resetPasswordExpires: Date,
    perfil:             {type:Schema.ObjectId, ref:"Perfil"},
    empresa:            {type:Schema.ObjectId, ref:"Empresa"},
    sucursal:           {type:Schema.ObjectId, ref:"Sucursal"},
    modulo:             Number
  });

//mapeo de la relacion a traves del plugin mongoose-relationship
// EjecutivoSchema.plugin(relationship, { relationshipPathName:'perfil' });
// EjecutivoSchema.plugin(relationship, { relationshipPathName:'empresa'});
// EjecutivoSchema.plugin(relationship, { relationshipPathName:'sucursal'});

//generando hash
EjecutivoSchema.methods.generateHash = function(contrasena) {
  return bcrypt.hashSync(contrasena, bcrypt.genSaltSync(8), null);
};

//validando hash
EjecutivoSchema.methods.validPassword = function(contrasena) {
    return bcrypt.compareSync(contrasena, this.contrasena);
};

var Ejecutivo = models.model('Ejecutivo', EjecutivoSchema);

module.exports = Ejecutivo;
