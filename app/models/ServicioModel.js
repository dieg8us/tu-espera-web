//Conexion y libreria mongoose
var models = require("../config/dbconnection"),
    Schema = models.Schema;

    //objeto modelo mongoose
    var ServicioSchema = new Schema({
        nombre:             String,
        descripcion:        String,
        fechaCreacion:      Date,
        fechaModificacion:  Date,
        flagEliminado:      Boolean,
        empresa:            { type:Schema.ObjectId, ref:"Empresa"},
        ejecutivo:          { type:Schema.ObjectId, ref:"Ejecutivo"},
        sucursal:           { type:Schema.ObjectId, ref:"Sucursal"}
        //relación de uno a muchos (una sucursal muchos servicios)
        // atencion:           [{ type:Schema.ObjectId, ref:"Atencion" }]
    });

//ServicioSchema.plugin(relationship, {relationshipPathName: 'sucursal'});

var Servicio = models.model('Servicio', ServicioSchema);

module.exports = Servicio;
