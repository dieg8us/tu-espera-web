//Conexion y libreria mongoose
var models = require("../config/dbconnection"),
    Schema = models.Schema,
    relationship = require("mongoose-relationship");

    //objeto modelo mongoose
    var ComunaSchema = new Schema({
        nombre:       String,
        region:       [{ type:Schema.ObjectId, ref:"Region" }]
    });

//ComunaSchema.plugin(relationship, {relationshipPathName: 'region'});

var Comuna = models.model('Comuna', ComunaSchema);

module.exports = Comuna;
