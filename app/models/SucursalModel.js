//Conexion y libreria mongoose
var models = require("../config/dbconnection"),
    Schema = models.Schema,
    relationship = require("mongoose-relationship");

    //objeto modelo mongoose
    var SucursalSchema = new Schema({
        nombre:             String,
        calle:              String,
        nroCalle:           String,
        telefono:           String,
        fax:                String,
        fechaCreacion:      Date,
        fechaModificacion:  Date,
        flagEliminado:      Boolean,
        horario:            [{ type:Schema.ObjectId, ref:"Horario" }],
        ejecutivo:          [{ type:Schema.ObjectId, ref:"Ejecutivo" }],
        direccion:          [{ type:Schema.ObjectId, ref:"Direccion" }],
        empresa:            [{ type:Schema.ObjectId, ref:"Empresa" }]
        // region:             [{ type:Schema.ObjectId, ref:"Region" }],
        // comuna:             [{ type:Schema.ObjectId, ref:"Comuna" }]
    });

// mapeo de la relacion a traves del plugin mongoose-relationship
// SucursalSchema.plugin(relationship, { relationshipPathName:'empresa'});

var Sucursal = models.model('Sucursal', SucursalSchema);

module.exports = Sucursal;
