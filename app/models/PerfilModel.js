//Conexion y libreria mongoose
var models = require("../config/dbconnection"),
    Schema = models.Schema,
    relationship = require("mongoose-relationship");

    //objeto modelo mongoose
    var perfilSchema = new Schema({
        nombre:             String,
        descripcion:        String,
        fechaCreacion:      Date,
        fechaModificacion:  Date,
        flagEliminado:      Boolean,
        permiso:            [{ type:Schema.ObjectId, ref: "Permiso"}],
        ejecutivo:          [{ type:Schema.ObjectId, ref:"Ejecutivo"}]
    });

var Perfil = models.model('Perfil', perfilSchema);

module.exports = Perfil;
