var Atencion  = require("../models/AtencionModel"),
    Sucursal  = require("../models/SucursalModel"),
    Ejecutivo = require("../models/EjecutivoModel");
    //functions = require("../config/functions");

// controlador de atenciones en tu-espera
var atencionController = function(app, passport, io) {

      // todas las atenciones realizadas
    var getAtenciones = function(req, res, next) {

      var options = { select: 'bool' };

      Atencion.dataTable(req.query, options, function(err, data){
        if (err) return next(err);
        res.send(data);
      });
    }

    // carga datos de ejecutivo para gestionar
    // sucursal
    var loadAtencion = function(req, res) {

      var user = req.user;

      Ejecutivo.findById(req.user.id).populate('sucursal').exec( function(err, ejecutivo) {
          if(!err) {
            res.render('atencion/atencion',{ ejecutivo: ejecutivo });
          }
          else {
            req.flash('error', 'error en la consulta de sucursal ' + err);
            res.redirect('atencion/atencion');
          }
      });
    };

    var insAtencion = function(req, res) {

      var numeroAtencion = req.body.hcli_transito;
      var fecha = Date.now();
      var hora = Date.now();
      var duracionAtencion = 5;
      var estado = 1;
      var estadoAtencion = req.body.estado;
      var comentario = req.body.comentario;
      var sucursal = req.body.nombre_sucursal;
      var ejecutivo_atencion = req.user.id;

      // socket.emit('sucursal', sucursal);

      Atencion.findOne({ numeroAtencion: numeroAtencion }, function(err, atencion) {
          if(!err){

              // datos de actualizacion
              atencion.fecha = fecha;
              atencion.hora = hora;
              atencion.duracionAtencion = duracionAtencion;
              atencion.estado = estado;
              atencion.estadoAtencion = estadoAtencion;
              atencion.comentario = comentario;
              atencion.sucursal = sucursal;
              atencion.ejecutivo = ejecutivo_atencion;

              atencion.save(function(err) {
                  if(!err) {
                    req.flash('success', 'Siguiente Cliente' );
                    res.redirect('/atencion');
                  } else {
                    req.flash('error', 'error ingresando la atención' + err );
                    res.redirect('/atencion');
                  }
              });

          } else {
            req.flash('error', 'error ingresando la atención' + err );
            res.redirect('atencion/atencion');
          }
      });

      // res.redirect('atencion/atencion');
      // buscar el ultimo numero de atencion
      //Atencion.find().sort({ numeroAtencion: -1 }).limit(1).exec(function(err, nAtencion) {
      // });

    };

    var infatenciones = function(req, res) {
        res.render('informes/infatenciones');
    };

    var visoratenciones = function(req, res) {

        var user = req.user;

        Ejecutivo.findById(req.user.id).populate('sucursal').exec( function(err, ejecutivo) {
          if(!err) {
            res.render('atencion/atencion_sucursal',{ ejecutivo: ejecutivo });
          }
          else {
            // req.flash('error', 'error en la consulta de sucursal ' + err);
            res.redirect('index');
          }
        });
    };

    // rutas
    // cargar pagina de atenciones
    app.get("/atencion", isLoggedIn, loadAtencion);
    // testing atencion
    app.post('/atencion', isLoggedIn, insAtencion);
    // todas las atenciones
    app.get('/getatenciones', getAtenciones);
    // info atenciones
    app.get('/infatenciones', isLoggedIn, infatenciones);
    // visor de atenciones
    app.get('/visoratenciones', visoratenciones);

    // middleware para asegurar que el usuario esta logeado a
    // la aplicacion
    function isLoggedIn(req, res, next) {
      // si el usuario esta logeado a la aplicacion
      // continua
      if (req.isAuthenticated())
        return next();

      // si no esta logeado se redirecciona al index de la
      // aplicacion
      res.redirect('/');
    }
};

module.exports = atencionController;
