var Permiso = require('../models/PermisoModel');

var permisoController = function(app, passport){

    // funcion que rescata datos del
    // modelo Permisos desde la bd y los
    // filtra para que los datos eliminados por el
    // usuario no se visualicen
    var getPermisos = function(req, res, next){

      // opciones de seleccion de datos
       var options = { select: "bool", conditions: { flagEliminado: false} };

         // consulta a la bd mongo a traves de mongoose-datatable
         Permiso.dataTable(req.query, options, function(err, permisos) {
           // se valida que se devualvan datos para el jquery-datatable
           if (err) return next(err);
           // se retornan datos de permisos
           res.send(permisos);
         });
    }

    // funcion para carga la pagina de permisos
    // con el renderizado de mensajes flash
    var loadInsPermiso = function(req, res){
        // mensaje flash
        var flash = req.flash();
        // renderizando pagina de permisos
        res.render('permisos/permisos', {flash: flash});
    }

    // funcion para ingresar nuevos permisos
    var insPermiso = function(req, res){

        var nombrePagina = "";
        var ingreso = "";
        var url = "";
        var fechaCreacion = Date.now();
        var usuario = req.user.id;

        if(req.body.nombrePagina != null){ nombrePagina = req.body.nombrePagina};
        if(req.body.ingreso != null){ ingreso = req.body.ingreso};
        if(req.body.urlPagina != null){ url = req.body.urlPagina};

        Permiso.findOne({ nombrePagina: nombrePagina, ingreso: ingreso,
                        urlPagina: url }, function(err, permiso) {

            // validando si existe el permiso
            if(permiso != null){
                // mensaje flash
                req.flash('error', 'El permiso ya se encuentra ingresado');
                // retornando pagina con mensaje
                return res.redirect('/permisos');
            } else {
              // validando si hay error en la consulta
              if(!err){
                debugger;

                var usuario = req.user;

                // ingresando el permiso
                var newPermiso = new Permiso();

                newPermiso.nombrePagina = nombrePagina;
                newPermiso.ingreso = ingreso;
                newPermiso.urlPagina = url;
                newPermiso.flagEliminado = false;
                newPermiso.fechaCreacion = fechaCreacion;
                newPermiso.ejecutivo = usuario;

                newPermiso.save(function(err){

                    //validando el ingreso
                    if(err){
                      // mensaje flash
                      req.flash('error', 'Error ingresando los datos' + err);
                      // retornando a pagina de permisos con mensaje de error
                      return res.redirect('/permisos');
                    } else {
                      // mensaje flash
                      req.flash('success', 'Ingreso de permiso OK!');
                      // retornando a pagina de permisos con mensajes
                      res.redirect('/permisos');
                    }
                });

              } else {
                 // mensaje flash
                 req.flash('error', 'Error validando datos de ingreso' + err);
                 // retornando a pagina de permisos con mensajes
                 return res.redirect('/permisos');
              }
            }
        });
    }

    // funcion de actualizacion de permisos
    var updPermiso = function(req, res){

      // nombre pagina
      var nPagina;
      // ingreso
      var ingreso;
      // url pagina
      var urlPagina;
      // id permiso
      var idPermiso;

      // validacion de nombre de pagina
      if(req.body.updNombrePagina != null){nPagina = req.body.updNombrePagina};
      // validacion de ingreso de pagina
      if(req.body.UpdIngreso != null){ingreso = req.body.UpdIngreso};
      // validacion de url de pagina
      if(req.body.updUrlPagina != null){urlPagina = req.body.updUrlPagina};
      // validacion de id de permiso
      if(req.body.idPermiso != null){idPermiso = req.body.idPermiso};

      Permiso.findById(idPermiso, function(err, uPermiso){

          if(!err){

            // actualizando campos
            uPermiso.nombrePagina = nPagina;
            uPermiso.ingreso = ingreso;
            uPermiso.urlPagina = urlPagina;

            // grabando campos
            return uPermiso.save(function(err){
                if(!err){
                  req.flash('success', 'Permiso Actualizado!');
                  return res.redirect('/permisos');
                } else {
                  req.flash('error', 'Error actualizando los datos ' + err);
                  return res.redirect('/permisos');
                }
            });

          } else{

            // error de consulta de datos de permiso
            console.log('error' + err);
            return res.redirect('/permisos');
          }
      });
    }

    // funcion de eliminacion de permisos
    var delPermiso = function(req, res){

       debugger;
       // variables
       var idPermiso = req.params.id;

       return Permiso.findById(idPermiso, function(err, permiso){

            debugger;

            // validacion de existencia
            if(!err){

              permiso.flagEliminado = true;

              permiso.save(function(err){

                  // validacion de actualizacion
                  if(!err){
                    req.flash('success', 'Permiso eliminado!');
                    res.redirect('/permisos');
                  } else {
                    req.flash('error', 'Error eliminando registro ' + err);
                    res.redirect('/permisos');
                  }
              });

            } else {
              req.flash('error', 'Error eliminando registro ' + err);
              res.redirect('/permisos');
            }
       });
    }

    // rutas

    // cargando pagina de permisos
    app.get('/permisos', isLoggedIn, loadInsPermiso);

    // get permisos datatable
    app.get('/getpermisos', isLoggedIn, getPermisos);
    // consultando permisos
    //app.post('/permisos', findPermiso);

    // ingresando permisos
    app.post('/inspermisos', isLoggedIn, insPermiso);

    // editando permiso
    app.post('/updpermiso', isLoggedIn, updPermiso);

    // eliminando permiso
    app.get('/delpermiso/:id', isLoggedIn, delPermiso);

    // middleware para asegurar que el usuario esta logeado a
    // la aplicacion
    function isLoggedIn(req, res, next) {
      // si el usuario esta logeado a la aplicacion
      // continua
      if (req.isAuthenticated())
        return next();

      // si no esta logeado se redirecciona al index de la
      // aplicacion
      res.redirect('/');
    }
}

module.exports = permisoController;
