var Permiso = require("../models/PermisoModel"),
    Perfil  = require("../models/PerfilModel");
var _ = require("underscore");

//Controlador Home
var homeController = function (server, passport) {
  console.log('cargando controlador home');

  //Raiz del controlador Home
  server.get('/home', isLoggedIn, function (req, res) {
    Perfil.findById(req.user.perfil, function(err, perfil){
       if(!err) {
          if(perfil.nombre == 'ejecutivo'){
             res.redirect('/atencion');
          } else {
            res.redirect('/ejecutivos');
            // res.render('home');
          }
       } else {
          req.flash('error', 'Usuario sin perfil definido');
          res.redirect('/');
       }
    });

  });

  // middleware para asegurar que el usuario esta logeado a
  // la aplicacion
  function isLoggedIn(req, res, next) {

    // si el usuario esta logeado a la aplicacion
    // continua
    if (req.isAuthenticated())
      return next();

    // si no esta logeado se redirecciona al index de la
    // aplicacion
    res.redirect('/');
  }

};

//Se exporta el controlador
module.exports = homeController;
