// controlador para funciones relacionadas con
// servicios de empresa

// import de modulos
var Empresa =   require('../models/EmpresaModel'),
    Rubro =     require('../models/RubroModel'),
    Sucursal =  require('../models/SucursalModel'),
    Region =    require('../models/RegionModel'),
    Comuna =    require('../models/ComunaModel'),
    Horario =   require('../models/HorarioModel'),
    Direccion = require('../models/DireccionModel'),
    Servicio =  require('../models/ServicioModel'),
    Async =     require('async');

// controlador empresa
var empresaController = function(app, passport) {

  // funcion que rescata datos del
  // modelo Rubro desde la bd y los
  // filtra para que los datos eliminados por el
  // usuario no se visualicen
  var getEmpresas = function(req, res, next){

    // opciones de seleccion de datos
    var options = { select: "bool", conditions: { flagEliminado: false} };

    // consulta a la bd mongo a traves de mongoose-datatable

    Empresa.dataTable(req.query, options, function(err, empresas) {
      // se valida que se devualvan datos para el jquery-datatable
      if (err) return next(err);
      // se retornan datos de rubros
      res.send(empresas);

    });
  }

  // selecion de rubros
  var loadNuevaEmpresa = function(req, res){

    var rubros = Rubro.find({flagEliminado : false},function(err, rubros){

      if(!err) {
        // mensaje flash dinamico
        var flash = req.flash();
        // se retornan todos los rubros
        res.render('empresas/nuevaEmpresa', {rubros: rubros, flash: flash});
      } else {
        res.statusCode = 500;
        //console.log('Error! consultando los datos' + err);
        return res.redirect('/nuevaEmpresa');
      }
    });
  };

  // seleccion de empresas
  var loadEmpresas = function(req, res){

    // retornando todas las empresas
    Empresa.find(function(err, empresas){

      if(!err) {

        // mensaje flash dinamico
        var flash = req.flash();
        // se retornan todos los ejecutivos
        res.render('empresas/empresas', {empresas: empresas, flash: flash});
      } else {
        res.statusCode = 500;
        //console.log('Error! consultando los datos' + err);
        return res.redirect('/empresas');
      }
    });

  };

  // ingreso de una empresa
  var ingresoEmpresa = function(req, res) {

    // razonSocial
    var razonSocial = req.body.razonSocial;
    var telefonos = req.body.telefonos;
    var fax  =req.body.fax;
    // rut
    var rut = req.body.rut;
    var rubroId = req.body.hddIdRubro;
    // fecha creacion
    var fechaCreacion = Date.now();

    Empresa.findOne({ 'rut' : rut }, function(err, empresa){

      // error de la consulta a mongodb
      if(err){
        // mensaje de error
        req.flash('error', "Oops! Error!" + err);
        // redireccionando a pagina de ingreso de rubro
        res.redirect('/empresas');
      }
      // si existe la empresa
      if(empresa){
        // mensaje de warning
        req.flash('warning', 'Oops! Rubro ya existe dentro del sistema');
        // redireccionando a pagina de ingreso de empresas
        res.redirect('/empresas');
      } else {

        var newEmpresa= new Empresa()
        newEmpresa.rut= rut;
        newEmpresa.razonSocial= razonSocial;
        newEmpresa.fax= fax;
        newEmpresa.telefono= telefonos;
        newEmpresa.fechaCreacion=fechaCreacion;
        newEmpresa.flagEliminado = false;
        newEmpresa.ejecutivo = req.user.id;
        newEmpresa.rubro = rubroId;

        newEmpresa.save(function(err,empresa){

          // control de error
          if(err){
            // mensaje de error
            req.flash('error', 'Error! guardando registo: ' + err);
            // redireccionando a pagina de ingreso de empresa
            return res.redirect('/nuevaEmpresa');

          } else {

            // mensaje de ingreso OK
            req.flash('success', 'Empresa registrada exitosamente.');

            // redireccionando a pagina de ingreso de empresa
            return res.redirect('/empresas/'+empresa._id);
          }
        });
      }
    });
  };

  // seleccion de detalle de empresa
  var selDetalleEmpresa = function(req, res) {

    var idEmpresa = req.params.id;

    // Consultas Paralelas Javascript
    Async.parallel([

      // detalle empresa idEmpresa
      function(callback) {
        Empresa.findById(idEmpresa, function(err, empresa) {
            if(err){
              callback(err);
            }

            callback(null, empresa);
        });
      },

      // comunas
      function(callback) {
        Comuna.find({}, function(err, comunas) {
            if(err){
              callback(err);
            }
            callback(err, comunas);
        });
      },

      // regiones
      function(callback) {
        Region.find({}, function(err, regiones) {
            if(err){
              callback(err);
            }
            callback(err, regiones);
        });
      },

      // rubros
      function(callback) {
        Rubro.find({}, function(err, rubros) {
            if(err){
              callback(err);
            }
            callback(err, rubros);
        });
      },

      // sucursales
      function(callback) {
        Sucursal.find({ flagEliminado : false, empresa : idEmpresa }, function(err, sucursales) {
            if(err) {
              callback(err);
            }
            callback(err, sucursales);
        });
      },

      // horarios
      function(callback) {
        Horario.find({ flagEliminado : false }, function(err, horarios) {
          if(err){
            callback(err);
          }
          callback(null, horarios);
        });
      }

    ],

    // resultados consultas paralelas
    function(err, results) {
      // validacion error en las consultas
      if (err) {
        console.log(err);
        return res.send(400);
      }
      // validacion resultados sin datos
      if (results == null || results[0] == null) {
        return res.send(400);
      }

      // [0] = empresa
      // [1] = comunas
      // [2] = regiones
      // [3] = rubros
      // [4] = sucursales
      // [5] = horarios

      var flash = req.flash();

      return res.render('empresas/editarEmpresa',{
          empresa:    results[0],
          comunas:    results[1],
          regiones:   results[2],
          rubros:     results[3],
          sucursales: results[4],
          horarios:   results[5],
          flash:      flash
      });

    });

  };

  // actualizacion de empresa
  var updEmpresa = function(req, res) {

    var idEmpresa = req.body.id_empresa;

    // validar id de empresa
    return Empresa.findById(idEmpresa, function(err, empresa){
      // rut
      if(req.body.rut !== null) {empresa.rut = req.body.rut};
      // razonSocial
      if(req.body.razonSocial !== null) {empresa.razonSocial = req.body.razonSocial};
      // telefono
      if(req.body.telefonos !== null) {empresa.telefono = req.body.telefonos};
      // fax
      if(req.body.fax !== null) {empresa.fax = req.body.fax};
      // activo
      empresa.flagEliminado=false;
      empresa.fechaModificacion =  Date.now();
      empresa.ejecutivo =  req.user.id;
      empresa.rubro._id = req.body.hddIdRubro;
      //falta rubro

      return empresa.save(function(err) {

        // se valida si existe error
        if(!err){

          // mensaje flash
          req.flash('success', 'Empresa actualizada!');
          return res.redirect('/empresas');

        } else {

          if(err.name == 'Validation error'){

            // mensaje flash
            req.flash('error', 'Error de validacion');
            return res.redirect('/empresas');

          } else {

            // mensaje flash
            req.flash('error','Error de Servidor' + err);
            return req.redirect('/empresas');

          } // validation error
        } // err
      });
    });
  };

  // seleccion de comuna por region
  var ObtenerComunasPorIdRegion= function(req, res){
    var idRegion = req.body.idRegion;

    Comuna.find({region : idRegion},function(err, comunas){

      if(!err) {
        //debugger;
        var jsonComunas = JSON.stringify(comunas);
        res.writeHead(200, {'content-type':'text/json'});
        return res.end(jsonComunas);
      } else {
        return res.statusCode = 500;
      }
    });
  };

  // seleccion de empresa aplicacion movil (Api)
  var empresasMovil = function(req, res) {

    // Sucursal.find({}).populate('empresa').exec(function(err, sucursales) {
    //   debugger;
    // });
    debugger;
    // id empresa
    var mongoose = require('mongoose');
    var rubroId = mongoose.Types.ObjectId(req.body.rubroId);

    // retornando todas las empresas
    Empresa.find({rubro: rubroId}, function(err, empresas) {
        if(!err) {
          res.jsonp({ empresas: empresas });
        } else {
          res.statusCode = 500;
          res.status(500).jsonp({ error: 'message' });
        }
    });

    // Empresa.find({ }, function(err, empresas).populate('rubro').exec(function() ) {
    //
    //   if(!err) {
    //
    //     // mensaje flash dinamico
    //     // var flash = req.flash();
    //     // se retornan todos los ejecutivos
    //     res.jsonp({ empresas: empresas });
    //
    //   } else {
    //     res.statusCode = 500;
    //     //console.log('Error! consultando los datos' + err);
    //     res.status(500).jsonp({ error: 'message' });
    //   }
    // });
  };

  // get empresa datatable
  app.get('/getempresas', isLoggedIn, getEmpresas);
  // seleciona todos las empresas
  app.get('/empresas', isLoggedIn, loadEmpresas);
  // seleciona todos las empresas movil
  app.post('/empresasmovil', empresasMovil);
  // editar empresa
  app.get('/empresas/:id', isLoggedIn, selDetalleEmpresa);
  // ingreso a la pagina de nueva empresa
  app.get('/nuevaEmpresa', isLoggedIn, loadNuevaEmpresa);
  // nueva empresa
  app.post('/nuevaEmpresa', isLoggedIn, ingresoEmpresa);
  // obtiene comunas por id de region
  app.post('/empresas/ObtenerComunasPorIdRegion', isLoggedIn, ObtenerComunasPorIdRegion);
  // actualizar empresa
  app.post('/updEmpresa', isLoggedIn, updEmpresa);

  // middleware para asegurar que el usuario esta logeado a
  // la aplicacion
  function isLoggedIn(req, res, next) {
    // si el usuario esta logeado a la aplicacion
    // continua
    if (req.isAuthenticated())
      return next();

      // si no esta logeado se redirecciona al index de la
      // aplicacion
      res.redirect('/');
    }

};

  module.exports = empresaController;
