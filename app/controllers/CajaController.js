var Caja  = require('../models/CajaModel.js');

var cajaController = function(app, passport) {

    // carga de cajas
    var getCajas = function(req, res, next) {

        debugger;
        // parametro de id sucursal
        var sucursal = req.params.idsucursal;

        // opciones de datatable
        var options = { select: 'bool' , conditions:{ sucursal: sucursal } };

        // retorno de cajas
        Caja.dataTable(req.query, options, function(err, cajas) {
            // validacion de consulta
            debugger;
            if(!err) {
              // retorno de cajas
              res.send({ cajas: cajas });
            }
        });
    }

    // ingreso de cajas
    var insCaja = function(req, res) {

        // id_empresa
        var idEmpresa = req.body.idEmpresaCaja;

        // variables de ingreso
        var numero = req.body.numeroCaja;
        var sucursal = req.body.idcajasucursal;
        var fechaIngreso = Date.now();
        var usuario = req.user.id;

        Caja.findOne({ numero: numero, sucursal: sucursal }, function(err, caja) {

            // validacion de error en la consulta de sucursal
            if(err) {

                // mensaje de error
                req.flash('error', 'error no se pudo validar la caja que se desea ingresar');
                res.redirect('/empresas/' + idEmpresa );

            } else {

               // validacion si existe caja a ingresar
               if(caja == null) {

                    // guardando caja

                    var newCaja = new Caja();
                    newCaja.numero = numero;
                    newCaja.sucursal = sucursal;
                    newCaja.fechaIngreso = fechaIngreso;
                    newCaja.ejecutivo = usuario;

                    newCaja.save(function(err, icaja) {

                        debugger;

                       // validacion de error de ingreso
                       if(!err) {

                          // mensaje de ingreso caja
                          req.flash('success','caja ingresada satisfactoriamente');
                          res.redirect('/empresas/' + idEmpresa );

                       }
                       else {

                           // mensaje de error de ingreso de caja
                           req.flash('error', 'error en el ingreso de caja' + err);
                           res.redirect('/empresas/' + idEmpresa );

                       }
                    });

               } else {

                 // mensaje de error de ingreso de caja
                 req.flash('error', 'Numero de caja ya ingresada en sucursal');
                 res.redirect('/empresas/' + idEmpresa );

               }
            }
        });
    }

    //rutas
    app.get('/empresas/sucursal/getcajas/:idsucursal', isLoggedIn, getCajas);

    app.post('/empresas/sucursal/inscaja', isLoggedIn, insCaja);

    // middleware para asegurar que el usuario esta logeado a
    // la aplicacion
    function isLoggedIn(req, res, next) {
      // si el usuario esta logeado a la aplicacion
      // continua
      if (req.isAuthenticated())
        return next();

      // si no esta logeado se redirecciona al index de la
      // aplicacion
      res.redirect('/');
    }
};

module.exports = cajaController;
