var Persona = require('../models/PersonaModel');

var personaController = function(app, passport) {

    var inspersona = function(req, res) {
        // ingreso de usuario
        var nombre = req.body.nombre;
        var apellido = req.body.apellido;
        var rut = req.body.rut;
        var email = req.body.email;
        var usuario = req.body.usuario;
        var password = req.body.password;

        Persona.findOne({ usuario: usuario }, function(err, persona) {
            // validacion de error de consulta
            if(!err) {
              // validacion de persona ya existente
              if(persona != null) {
                res.jsonp({ message: 'Usuario ya existe dentro del sistema ', pass: 0 });
              } else {
                // ingreso de nueva persona
                var newPersona = new Persona();

                newPersona.nombre = nombre;
                newPersona.apellido = apellido;
                newPersona.rut = rut;
                newPersona.email = email;
                newPersona.usuario = usuario;
                newPersona.password = password;

                // guardando persona
                newPersona.save(function(err, nPersona){
                  if(!err) {
                      // retornando resultado de ingreso OK
                      res.jsonp({ message: 'Usuario Ingresado al sistema', pass: 1 , persona: nPersona});
                  } else {
                      // retornando resultados de NO ingreso
                      res.jsonp({ message: 'Error en el ingreso de usuario ' + err, pass: 0});
                  }
                });
              }
            }
        });
    };

    var valpersona = function(req, res) {
        // usuario y password
        var usuario = req.body.usuario;
        var password = req.body.password;

        // buscando uusario
        Persona.findOne({ usuario: usuario, password: password}, function(err, usuario) {
           if(!err) {
             // retorno de usuario completo
             if(usuario != null) {
               res.jsonp({ persona: usuario, pass: 1, message: 'success' });
             } else {
               res.jsonp({ persona: usuario, pass: 0, message: 'error' });
             }
           }
        });
    };

    // rutas
    // ingreso de persona
    app.post('/api/inspersona', inspersona);
    // valida persona
    app.post('/api/getpersona', valpersona);
};

module.exports = personaController;
