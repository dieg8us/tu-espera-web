var Rubro = require('../models/RubroModel');

// controlador index
var rubroController = function(app, passport) {

   // funcion que rescata datos del
    // modelo Rubro desde la bd y los
    // filtra para que los datos eliminados por el
    // usuario no se visualicen
    var getRubros = function(req, res, next){

        // opciones de seleccion de datos
        var options = { select: "bool", conditions: { flagEliminado: false} };

        // consulta a la bd mongo a traves de mongoose-datatable

        Rubro.dataTable(req.query, options, function(err, rubros) {
          // se valida que se devualvan datos para el jquery-datatable
          if (err) return next(err);
          // se retornan datos de rubros
          res.send(rubros);

        });
    }

  // funcion de ingreso de un rubro
    var ingresoRubro = function(req, res) {

        // nombre
        var nombre = req.body.nom_rubro;
        // apellido
        var descripcion = req.body.descripcion;

        // fecha creacion
        var fechaCreacion = Date.now();

        Rubro.findOne({ 'nombre' : nombre }, function(err, rubro){

             // error de la consulta a mongodb
            if(err){
                // mensaje de error
                req.flash('error', "Oops! Error!" + err);
                // redireccionando a pagina de ingreso de rubro
                res.redirect('/rubros');
            }
            // si existe el rubro
            if(rubro){
                // mensaje de warning
                req.flash('warning', 'Oops! Rubro ya existe dentro del sistema');
                // redireccionando a pagina de ingreso de rubro
                res.redirect('/rubros');
            } else {

          var newRubro = new Rubro()
              newRubro.nombre= nombre;
              newRubro.tipo= descripcion;
              newRubro.fechaCreacion=fechaCreacion;
              newRubro.flagEliminado = false;
              newRubro.ejecutivo = req.usuario;

              newRubro.save(function(err){

                  // control de error
                   if(err){
                       // mensaje de error
                       req.flash('error', 'Error! guardando registo: ' + err);
                       // redireccionando a pagina de ingreso de rubro
                       return res.redirect('/rubros');

                   } else {

                       // mensaje de ingreso OK
                       req.flash('success', 'Rubro registrado exitosamente.');
                       // redireccionando a pagina de ingreso de rubro
                       return res.redirect('/rubros');
                  }
               });
             }
        });
    };

//validado
     var loadRubros = function(req, res){

        // retornando todos los ejecutivos
        Rubro.find(function(err, rubros) {

            if(!err) {

              // mensaje flash dinamico
              var flash = req.flash();
              // se retornan todos los ejecutivos
              res.render('rubros/rubros', {rubros: rubros, flash: flash});
            } else {
              res.statusCode = 500;
              //console.log('Error! consultando los datos' + err);
              return res.redirect('/rubros');
            }
        }) ;

    };
    //validado
    var delRubro = function(req, res){

      // validar si el usuario existe
      return Rubro.findById(req.params.id, function(err, rubro){
            // eliminando rubro
            return rubro.remove(function(err){
              console.log('rubro eliminado');

              // se valida error de eliminacion
              if(!err){
                // mensaje flash

                Rubro.find(function(err, rubros){

                  // retornando los ejecutivos restantes
                  if(!err){
                    req.flash('success','Rubro eliminado!');
                    return res.redirect('/rubros');
                  } else {
                    req.flash('error','Error consultando rubros!');
                    return res.redirect('/rubros');
                  }

                });

              } else {
                //mensaje flash
                req.flash('error', 'Error de Servidor' + err);
                return res.redirect('/rubros');
              }

            });

      });
    };

    var updRubro = function(req, res){

      // validar id de rubro
      return Rubro.findById(req.body.idRubro, function(err, rubro){
            // nombre
            if(req.body.updNombre !== null) {rubro.nombre = req.body.updNombre};
            // descripcion
            if(req.body.updDescripcion !== null) {rubro.tipo = req.body.updDescripcion};

            rubro.fechaModificacion= Date.now();

            return rubro.save(function(err) {

              // se valida si existe error
              if(!err){

                // mensaje flash
                req.flash('success', 'Rubro actualizado!');
                return res.redirect('/rubros');

              } else {

                if(err.name == 'Validation error'){

                  // mensaje flash
                  req.flash('error', 'Error de validacion');
                  return res.redirect('/rubros');

                } else {

                  // mensaje flash
                  req.flash('error','Error de Servidor' + err);
                  return req.redirect('/rubros');

                } // validation error
              } // err
          });
      });
    };

    // seleccion de rubros aplicacion movil (Api)
    var rubrosMovil = function(req, res) {

      // retornando todos los rubros
      Rubro.find(function(err, rubros) {
          if(!err) {
            res.jsonp({ rubros: rubros });
          } else {
            res.statusCode = 500;
            res.status(500).jsonp({ error: 'message' });
          }
      });
    };

    // rutas
    // get perfil datatable
    app.get('/getrubros', isLoggedIn, getRubros);

    // seleciona todos los rubros
    app.get('/rubros', isLoggedIn, loadRubros);

     // ingreso de nuevo perfil
    app.post('/rubros', isLoggedIn, ingresoRubro);

    // eliminar ejecutivo
     app.get('/eliminarRubro/:id',isLoggedIn, delRubro);

     // actualizar rubro
     app.post('/updRubro', isLoggedIn, updRubro);

     // seleciona todos los rubros movil
     app.get('/rubrosmovil', rubrosMovil);

     // ingreso a la pagina de rubro
    app.get('/rubros', isLoggedIn, function(req, res){

       // seteando variable de mensajes al cargar la pagina de ingreso
       // de ejecutivos
       var flash = req.flash();
       // renderizando pagina de rubro
       res.render('rubros', { flash: flash });

    });

    // middleware para asegurar que el usuario esta logeado a
    // la aplicacion
    function isLoggedIn(req, res, next) {

      // si el usuario esta logeado a la aplicacion
      // continua
      if (req.isAuthenticated())
        return next();

      // si no esta logeado se redirecciona al index de la
      // aplicacion
      res.redirect('/');
    }

};

module.exports = rubroController;
