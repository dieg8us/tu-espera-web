var Perfil = require("../models/PerfilModel");

var perfilController = function(app, passport){

    // funcion que rescata datos del
    // modelo Perfil desde la bd y los
    // filtra para que los datos eliminados por el
    // usuario no se visualicen
    var getPerfiles = function(req, res, next){

        // opciones de seleccion de datos
        var options = { select: "bool", conditions: { flagEliminado: false} };

        // consulta a la bd mongo a traves de mongoose-datatable
        Perfil.dataTable(req.query, options, function(err, perfiles) {
          // se valida que se devualvan datos para el jquery-datatable
          if (err) return next(err);
          // se retornan datos de permisos
          res.send(perfiles);

        });
    }

    // funcion que inicia la pagina de nuevo perfil
    var loadInsPerfil = function(req, res){
        // mensaje flash
        var flash = req.flash();
        // renderizando pagina de ingreso de nuevo perfil
        res.render('perfiles/perfiles',{flash: flash});
    }

    // funcion para el ingreso de nuevo perfil
    var insPerfil = function(req, res){

        // variables
        var nom_perfil;
        var descripcion;

        // validando campos de perfil
        if(req.body.nom_perfil != null) {nom_perfil = req.body.nom_perfil};
        if(req.body.descripcion != null) {descripcion = req.body.descripcion};

        Perfil.find({ nombre: nom_perfil, flagEliminado: false }, function(err, perfil){

            // validacion de la consulta a base de datos
            if(!err){

                // validacion si existe dentro de la base de datos
                if(perfil.length >= 1 ){
                    req.flash('error', 'Error el perfil que ya existe dentro de la base de datos');
                    res.redirect('/perfiles');
                } else {
                  // nuevo perfil
                  newPerfil = new Perfil();

                  // nombre del perfil
                  newPerfil.nombre = nom_perfil;
                  // descripcion del perfil
                  newPerfil.descripcion = descripcion;
                  // fecha de ingreso del perfil
                  newPerfil.fechaCreacion = Date.now();
                  // no esta eliminado al ingresar
                  newPerfil.flagEliminado = false;
                  // usuario que ingresa el perfil
                  newPerfil.ejecutivo = req.user.id;

                  // guardando el perfil
                  newPerfil.save(function(err){

                    // validacion de ingreso de nuevo perfil
                    if(!err){
                      req.flash('success','Ingreso de perfil realizado');
                      res.redirect('/perfiles');
                    } else {
                      req.flash('error', 'Error en el ingreso de nuevo perfil ' + err);
                      res.redirect('/perfiles');
                    }
                  });
                }

            } else {
              req.flash('error', 'Error en la validacion de perfil' + err);
              res.redirect('/perfiles');
            }
        });
    }

    // funcion que edita datos de perfil
    var updPerfil = function(req, res){

        // verifica si el perfil existe

        var idPerfil = req.body.idPerfil;
        return Perfil.findById(idPerfil, function(err, perfil){

            // validacion de consulta
            if(!err){
              // validacion de que perfil sea mas de uno
              if(perfil){

                // variables de actualizacion
                var nombre;
                var descripcion;

                // se valida que el nombre de la actualizacion no venga nulo para actualizarlo
                if(req.body.updNombre != null){nombre = req.body.updNombre};
                // se valida que la descripcion del perfil no venga nulo para asignarlo
                if(req.body.updDescripcion != null){descripcion = req.body.updDescripcion};
                
                perfil.nombre = nombre;
                perfil.descripcion = descripcion;

                perfil.save(function(err){

                  // validacion de perfil
                  if(!err){
                    req.flash('success', 'Perfil actualizado!');
                    res.redirect('/perfiles');
                  } else {
                    req.flash('error', 'Error en la inserción de datos' + err);
                    res.redirect('/perfiles');
                  }
                });
              } else {
                req.flash('error', 'El perfil no existe para actualizarlo!');
                res.redirect('/perfiles');
              }
            } else {
              req.flash('error', 'Error en la consulta de datos ' + err);
              res.redirect('/perfiles');
            }
        });
    }

    var delPerfil = function(req, res){

        // valor del parametro
        var idPerfil = req.params.id;

        Perfil.findById(idPerfil, function(err, perfil){

            // se valida que el perfil exista
            if(perfil){

                perfil.flagEliminado = true;

                perfil.save(function(err){

                    // validando actualizacion del perfil eliminado
                    if(!err){
                      req.flash('success', 'Perfil eliminado!');
                      res.redirect('/perfiles');
                    } else {
                      req.flash('error', 'Error en la eliminación del Perfil ' + err);
                      res.redirect('/perfiles');
                    }
                });
            }

            // validando la consulta del perfil
            if(err){
                req.flash('error', 'Error en la eliminacion del Perfil ' + err);
                res.redirect('/perfiles');
            }

        });
    }

    // rutas

    // ingreso a pagina de perfiles
    app.get('/perfiles', isLoggedIn, loadInsPerfil);

    // ingreso de nuevo perfil
    app.post('/perfiles', isLoggedIn, insPerfil);

    // get perfil datatable
    app.get('/getperfiles', isLoggedIn, getPerfiles);

    // actualizando perfil
    app.post('/updperfil', isLoggedIn, updPerfil);

    // borrando perfil
    app.get('/delperfil/:id', isLoggedIn, delPerfil);

    // middleware para asegurar que el usuario esta logeado a
    // la aplicacion
    function isLoggedIn(req, res, next) {
      // si el usuario esta logeado a la aplicacion
      // continua
      if (req.isAuthenticated())
        return next();

      // si no esta logeado se redirecciona al index de la
      // aplicacion
      res.redirect('/');
    }
}

module.exports = perfilController;
