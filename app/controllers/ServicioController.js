// controlador para funciones relacionadas con
// servicios de empresa

// import de modulos
var Servicio = require('../models/ServicioModel');

// controlador
var servicioController = function(app, passport) {

	var getServicios = function(req, res) {

		// opciones de seleccion de datos
		var options = { select: "bool", conditions: { flagEliminado: false} };

		// consulta a la bd mongo a traves de mongoose-datatable
		Servicio.dataTable(req.query, options, function(err, servicios) {
			// se valida que se devualvan datos para el jquery-datatable
			if (err) return next(err);
			// se retornan datos
			res.send(servicios);

		});
	};

	// eliminar servicio
	var delServicio = function(req, res) {
		var arr = req.params.id.split(";");
		var idServicio = arr[1];
		var idEmpresa = arr[0];

		return Servicio.findById(idServicio, function(err, servicio){

			// eliminando servicio
			return servicio.remove(function(err){

				// se valida error de eliminacion
				if(!err){
					req.flash('success','Servicio eliminado!');
					return res.redirect('/empresas/'+idEmpresa);

				} else {
					//mensaje flash
					req.flash('error', 'Error de Servidor' + err);
					return res.redirect('/empresas/'+idEmpresa);
				}

			});

		});
	};

	// actualizar servicio
	var updServicio = function(req, res) {

		var idEmpresa = req.body.idEmpresaServicio_Actualizar;
		var idServicio = req.body.updIdServicio;
		// validar id de empresa
		return Servicio.findById(idServicio, function(err, servicio) {

			// se valida si existe error
			if(!err){
				// nombre
				if(req.body.updServicioNombre !== null) {servicio.nombre = req.body.updServicioNombre};
				// descripcion
				if(req.body.updServicioDescripcion !== null) {servicio.descripcion = req.body.updServicioDescripcion};
				// sucursal
				if(req.body.updCmbSucursal !== null) {servicio.sucursal = req.body.updCmbSucursal};
				// activo
				servicio.flagEliminado=false;
				servicio.fechaModificacion =  Date.now();
				servicio.ejecutivo =  req.user.id;
				//falta rubro

				return servicio.save(function(err) {

					// se valida si existe error
					if(!err) {
						// mensaje flash
						req.flash('success', 'Servicio actualizado!');
						return res.redirect('/empresas/'+idEmpresa);

					} else {

						if(err.name == 'Validation error') {
							// mensaje flash
							req.flash('error', 'Error de validacion');
							return res.redirect('/empresas/'+idEmpresa);

						} else {
							// mensaje flash
							req.flash('error','Error de Servidor' + err);
							return res.redirect('/empresas/'+idEmpresa);
						} // validation error
					} // err
				});
			}
			else {
				// mensaje flash
				req.flash('error','Error de Servidor' + err);
				return res.redirect('/empresas/'+idEmpresa);

			}
		});
	};

	// funcion de ingreso de una sucursal
	var ingresoServicio = function(req, res) {

		var id_empresa = req.body.idEmpresaServicio_Nueva;
		var nombre = req.body.servicioNombre;
		var descripcion = req.body.servicioDescripcion;
		var idSucursal  =req.body.cmbSucursal;
		var fechaCreacion = Date.now();

		Servicio.findOne({ 'nombre' : nombre ,'sucursal' : idSucursal }, function(err, servicio){

			// error de la consulta a mongodb
			if(err){
				// mensaje de error
				req.flash('error', "Oops! Error!" + err);
				// redireccionando a pagina de ingreso de rubro
				res.redirect('/empresas');
			}
			// si existe la empresa
			if(servicio){
				// mensaje de warning
				req.flash('warning', 'Oops! Servicio ya existe  dentro del sistema');
				// redireccionando a pagina de ingreso de empresas
				res.redirect('/empresas');
			} else {

				var newServicio= new Servicio()
				newServicio.nombre= nombre;
				newServicio.descripcion= descripcion;
				newServicio.fechaCreacion=fechaCreacion;
				newServicio.flagEliminado = false;
				newServicio.ejecutivo = req.user.id;
				newServicio.sucursal = idSucursal;
				newServicio.empresa = id_empresa;

				newServicio.save(function(err,servicio){

					// control de error
					if(err){
						// mensaje de error
						req.flash('error', 'Error! guardando registo: ' + err);
						// redireccionando a pagina de edicion de empresa
						return res.redirect('/empresas/'+id_empresa);

					} else {
						// mensaje flash
						req.flash('success', 'Servicio creado!');
						return res.redirect('/empresas/'+id_empresa);
					}
				});
			}
		});
	};

	// rutas
	// get empresa servicios datatable
	app.get('/empresas/getServicios/:id', isLoggedIn, getServicios);
	// nuevo servicio
	app.post('/empresas/nuevoServicio', isLoggedIn, ingresoServicio);
	// actualizar servicio
	app.post('/empresas/updServicio', isLoggedIn, updServicio);
	// eliminar servicio
	app.get('/empresas/eliminarServicio/:id',isLoggedIn, delServicio);

	// middleware para asegurar que el usuario esta logeado a
	// la aplicacion
	function isLoggedIn(req, res, next) {
		// si el usuario esta logeado a la aplicacion
		// continua
		if (req.isAuthenticated())
			return next();

		// si no esta logeado se redirecciona al index de la
		// aplicacion
		res.redirect('/');
	}
}

module.exports = servicioController;
