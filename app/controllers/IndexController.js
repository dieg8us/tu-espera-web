var Ejecutivo  = require("../models/EjecutivoModel"),
    crypto     = require('crypto'),
    nodemailer = require('nodemailer'),
    passport   = require('passport'),
    sgTransport = require('nodemailer-sendgrid-transport'),
    async      = require('async');

exports.index = function(req, res) {
  // mensajes flash
  var flash = req.flash();
  // render index
  res.render('index',{ flash: flash });
};

exports.login = function(req, res) {
  // render login
  res.render('index', { message: req.flash('loginMessage') });
};

exports.signin = function(passport) {
  // funcion para validar el usuario con passport en el
  // archivo ./config/passport.js -> local-login
  passport.authenticate('local-login', {
    successRedirect : '/ejecutivos', // redirecciona a home
		failureRedirect : '/login', // redirecciona a login
		failureFlash : true // permite mensajes flash en html
  });
};

exports.logout = function(req, res) {
  // logout user
  req.logout();
  res.redirect('/');
};

exports.getforgot = function(req, res) {
  // mensajes flash
  var flash = req.flash();

  res.render('forgot', {
    flash: flash,
    user: req.user
  });
};

exports.postforgot = function(req, res) {
  // consulta asincrona modo cascada
  async.waterfall([

    function(done) {
      crypto.randomBytes(20, function(err, buf) {
        var token = buf.toString('hex');
        done(err, token);
      });
    },
    function(token, done) {
      Ejecutivo.findOne({ usuario: req.body.username,
        email: req.body.email }, function(err, user) {
          debugger;
          if(!user) {
            req.flash('error', 'No existe la cuenta con el usuario y mail');
            return res.redirect('/forgot');
          }

          debugger;
          user.resetPasswordToken = token;
          user.resetPasswordExpires = Date.now() + 3600000; // 1 hora

          user.save(function(err) {
            done(err, token, user);
          });
      });
    },
    function(token, user, done) {

      var options = {
          auth: {
              api_user: 'diegous',
              api_key: '9281985dino$*'
          }
      }

      var smtpTransport = nodemailer.createTransport(sgTransport(options));
      // var smtpTransport = nodemailer.createTransport({
      //     service: 'SendGrid',
      //     auth: {
      //       api_user: 'tuespera',
      //       api_key: '9281985dino$*'
      //     }
      // });

      var mailOptions = {
        from: 'passwordreset@demo.com',
        to: user.email,
        subject: 'Node.js Password Reset',
        text: 'Estas recibiendo esto porque tu (o alguien más) solicito el reseteo de tu password para tu cuenta de tuEspera.\n\n' +
        'Por Favor has click en el siguiente link, o copia la siguiente dirección en tu navegador:\n\n' +
        'http://' + req.headers.host + '/reset/' + token + '\n\n' +
        'Si tu no has solicitado esto, por favor ignora este mail y tu contraseña seguira siendo la misma.\n'
      };
      smtpTransport.sendMail(mailOptions, function(err) {
        req.flash('info', 'Se ha envia un mail a tu dirección ' + user.email + ' con las intrucciones de reseteo de password.');
        done(err, 'done');
      });
    }
  ], function(err) {
      debugger;
      if(err) {
        req.flash('error', 'error ' + err);
        return res.redirect('/forgot');
      }

      res.redirect('/forgot');
  });
};

exports.getreset = function(req, res) {
  Ejecutivo.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
    if(!user) {
      req.flash('error', 'La solicitud de reseteo de Password es invalida o ya ha expirado.');
      return res.redirect('/forgot');
    }
    res.render('reset', {
      user: req.user
    });
  });
};

exports.postreset = function(req, res) {
  async.waterfall([

    function(done) {
      Ejecutivo.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires:{ $gt: Date.now() } }, function(err, user) {
        debugger;
        if(!user) {
          req.flash('error', 'Password reset token is invalid or has expired.');
          return res.redirect('back');
        }

        user.contrasena = user.generateHash(req.body.password);
        user.resetPasswordToken = undefined;
        user.resetPasswordExpires = undefined;

        user.save(function(err) {
          req.logIn(user, function(err) {
            done(err, user);
          });
        });
      });
    },
    function(user, done) {
      var options = {
          auth: {
              api_user: 'diegous',
              api_key: '9281985dino$*'
          }
      }

      var smtpTransport = nodemailer.createTransport(sgTransport(options));
      // var smtpTransport = nodemailer.createTransport({
      //   service: 'SendGrid',
      //   auth: {
      //     api_user: 'tuespera',
      //     api_key: '9281985dino$*'
      //   }
      // });
      var mailOptions = {
        from: 'passwordreset@demo.com',
        to: user.email,
        subject: 'Tú Password se ha modificado.',
        text: 'Hello,\n\n' +
          'Esta es una confirmación de que tu password para tu cuenta ' + user.usuario + ' se ha modificado.\n'
      };
      smtpTransport.sendMail(mailOptions, function(err) {
        req.flash('success', 'Logrado! Tú password se ha modificado.');
        done(err);
      });
    }
  ], function(err) {
    res.redirect('/');
  });
};

// controlador index
var indexController = function(app, passport) {
  console.log("cargando controlador");

  // ingreso a pantalla login
  app.get('/', function(req, res) {
    var flash = req.flash();
    res.render('index', { flash: flash });
  });

  // funcion de validacion de login
  app.get('/login', function(req, res){
    res.render('index', { message: req.flash('loginMessage') });
  });

  // funcion para validar el usuario con passport en el
  // archivo ./config/passport.js -> local-login
  app.post('/login', passport.authenticate('local-login', {
    successRedirect : '/ejecutivos', // redirecciona a home
		failureRedirect : '/login', // redirecciona a login
		failureFlash : true // permite mensajes flash en html
  }));

  // logout user
  app.get('/logout', function(req, res){
      req.logout();
      res.redirect('/');
  });

  // forgot password
  app.get('/forgot', function(req, res) {
    // mensajes flash
    var flash = req.flash();

    res.render('forgot', {
      flash: flash,
      user: req.user
    });
  });

  // envio mail de reseteo de password
  app.post('/forgot', function(req, res) {
    // consulta asincrona modo cascada
    async.waterfall([

      function(done) {
        crypto.randomBytes(20, function(err, buf) {
          var token = buf.toString('hex');
          done(err, token);
        });
      },
      function(token, done) {
        Ejecutivo.findOne({ usuario: req.body.username,
          email: req.body.email }, function(err, user) {
            debugger;
            if(!user) {
              req.flash('error', 'No existe la cuenta con el usuario y mail');
              return res.redirect('/forgot');
            }

            debugger;
            user.resetPasswordToken = token;
            user.resetPasswordExpires = Date.now() + 3600000; // 1 hora

            user.save(function(err) {
              done(err, token, user);
            });
        });
      },
      function(token, user, done) {
        var options = {
            auth: {
                api_user: 'diegous',
                api_key: '9281985dino$*'
            }
        }

        var smtpTransport = nodemailer.createTransport(sgTransport(options));
        // var smtpTransport = nodemailer.createTransport({
        //     service: 'SendGrid',
        //     auth: {
        //       api_user: 'tuespera',
        //       api_key: '9281985dino$*'
        //     }
        // });
        var mailOptions = {
          from: 'duribe@cobralex.cl',
          to: user.email,
          subject: 'Node.js Password Reset',
          text: 'Estas recibiendo esto porque tu (o alguien más) solicito el reseteo de tu password para tu cuenta de tuEspera.\n\n' +
          'Por Favor has click en el siguiente link, o copia la siguiente dirección en tu navegador:\n\n' +
          'http://' + req.headers.host + '/reset/' + token + '\n\n' +
          'Si tu no has solicitado esto, por favor ignora este mail y tu contraseña seguira siendo la misma.\n'
        };
        smtpTransport.sendMail(mailOptions, function(err) {
          req.flash('info', 'Se ha envia un mail a tu dirección ' + user.email + ' con las intrucciones de reseteo de password.');
          done(err, 'done');
        });
      }
    ], function(err) {
        debugger;
        if(err) {
          req.flash('error', 'error ' + err);
          return res.redirect('/forgot');
        }

        res.redirect('/forgot');
    });
  });

  // validacion de reset password
  app.get('/reset/:token', function(req, res) {
    Ejecutivo.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
      if(!user) {
        req.flash('error', 'La solicitud de reseteo de Password es invalida o ya ha expirado.');
        return res.redirect('/forgot');
      }
      res.render('reset', {
        user: req.user
      });
    });
  });

  // actualizar password
  app.post('/reset/:token', function(req, res) {

    async.waterfall([

      function(done) {
        Ejecutivo.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires:{ $gt: Date.now() } }, function(err, user) {
          debugger;
          if(!user) {
            req.flash('error', 'Password reset token is invalid or has expired.');
            return res.redirect('back');
          }

          user.contrasena = user.generateHash(req.body.password);
          user.resetPasswordToken = undefined;
          user.resetPasswordExpires = undefined;

          user.save(function(err) {
            req.logIn(user, function(err) {
              done(err, user);
            });
          });
        });
      },
      function(user, done) {
        var options = {
            auth: {
                api_user: 'diegous',
                api_key: '9281985dino$*'
            }
        }

        var smtpTransport = nodemailer.createTransport(sgTransport(options));
        // var smtpTransport = nodemailer.createTransport({
        //   service: 'SendGrid',
        //   auth: {
        //     api_user: 'tuespera',
        //     api_key: '9281985dino$*'
        //   }
        // });
        var mailOptions = {
          from: 'passwordreset@demo.com',
          to: user.email,
          subject: 'Tú Password se ha modificado.',
          text: 'Hello,\n\n' +
            'Esta es una confirmación de que tu password para tu cuenta ' + user.usuario + ' se ha modificado.\n'
        };
        smtpTransport.sendMail(mailOptions, function(err) {
          req.flash('success', 'Logrado! Tú password se ha modificado.');
          done(err);
        });
      }
    ], function(err) {
      res.redirect('/');
    });
  });

  // middleware para asegurar que el usuario esta logeado a
  // la aplicacion
  function isLoggedIn(req, res, next) {

    // si el usuario esta logeado a la aplicacion
    // continua
    if (req.isAuthenticated())
      return next();

    // si no esta logeado se redirecciona al index de la
    // aplicacion
    res.redirect('/');
  }

};

module.exports = indexController;
