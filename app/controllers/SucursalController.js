// controlador para funciones relacionadas con
// sucursales de empresa

// import de modulos
var Sucursal =  require('../models/SucursalModel'),
    Direccion = require('../models/DireccionModel'),
    Empresa =   require('../models/EmpresaModel'),
    Async =     require('async');

// controlador de sucursales
var sucursalController = function(app, passport) {

	  // obtener sucursales por id
    var getSucursales = function(req, res) {

      // id empresa
      var idEmpresa = req.params.id;

      Sucursal.find({empresa: idEmpresa}, function(err, sucursales) {
          if(!err){
             var options = { select: "bool", conditions: { sucursal: {$in: sucursales} } };
             Direccion.dataTable(req.query, options, function(err, direcciones) {
             });
          } else {
            res.statusCode = 500;
          }
      });

      // opciones de seleccion de datos
      var options = { select: "bool", conditions: { empresa: idEmpresa, flagEliminado: false } };

      // consulta a la bd mongo a traves de mongoose-datatable
      Sucursal.dataTable(req.query, options, function(err, sucursales) {
        // se valida que se devualvan datos para el jquery-datatable
        if (err) return next(err);
        // se retornan datos
        res.send(sucursales);

      });
    };

    // funcion de ingreso de una sucursal
    var ingresoSucursal = function(req, res) {

      // id_empresa
      var idEmpresa = req.body.idEmpresaSucursal;
      // horario sucursal
      var idHorario = req.body.horariosSucursal;
      // region sucursal
      var idRegion = req.body.hddIdRegionSucursal;
      // idcomuna
      var idComuna  =req.body.hddIdComunaSucursal;

      // razonSocial
      var nombreSucursal = req.body.nombreSucursal;
      // calle sucursal
      var calle = req.body.calleSucursal;
      // numero de calle
      var nroCalle = req.body.nroCalleSucursal;
      // telefono sucursal
      var telefono = req.body.telefonoSucursal;
      // fax
      var fax = req.body.faxSucursal;
      // fecha creacion de sucursal
      var fechaCreacion = Date.now();


      // latitud
      var latitud = req.body.latitud;
      // longitud
      var longitud = req.body.longitud;

      Sucursal.findOne({ 'nombre' : nombreSucursal ,'empresa' : idEmpresa }, function(err, empresa) {
        // error de la consulta a mongodb
        if(err) {
          // mensaje de error
          req.flash('error', "Oops! Error!" + err);
          // redireccionando a pagina de ingreso de rubro
          res.redirect('/empresas/' + idEmpresa);
        }
        // si existe la empresa
        if(empresa){
          // mensaje de warning
          req.flash('warning', 'Oops! Sucursal ya existe  dentro del sistema');
          // redireccionando a pagina de ingreso de empresas
          res.redirect('/empresas/' + idEmpresa);
        } else {

          var newSucursal= new Sucursal();

          // relaciones
          newSucursal.empresa = idEmpresa;
          newSucursal.horario = idHorario;

          // detalle
          newSucursal.nombre = nombreSucursal;
          newSucursal.fax = fax;
          newSucursal.telefono = telefono;
          newSucursal.fechaCreacion = fechaCreacion;
          newSucursal.flagEliminado = false;
          newSucursal.ejecutivo = req.user.id;
          newSucursal.empresa = idEmpresa;

          // guardando nueva sucursal
          newSucursal.save( function(err, sucursal) {

            // control de error
            if(err){
              // mensaje de error
              req.flash('error', 'Error! guardando registo: ' + err);
              // redireccionando a pagina de edicion de empresa
              return res.redirect('/empresas/'+ idEmpresa);

            } else {

              // id de nueva sucursal
              var idSucursal = sucursal._id;

              // creacion de nueva direccion enlazada a
              // nueva sucursal
              var newDireccion  = new Direccion()
              newDireccion.calle  = calle;
              newDireccion.numCalle = nroCalle;
              newDireccion.longitud = longitud;
              newDireccion.latitud  = latitud;
              newDireccion.flagEliminado  = false;
              newDireccion.ejecutivo  = req.user.id;
              newDireccion.sucursal = idSucursal;
              newDireccion.comuna = idComuna;
              newDireccion.fechaCreacion  = fechaCreacion;

              newDireccion.save(function(err, direccion) {

                // control de error
                if(err) {
                  // mensaje de error
                  req.flash('error', 'Error! guardando registo: ' + err);
                  // redireccionando a pagina de edicion de empresa
                  return res.redirect('/empresas/'+ idEmpresa);

                } else {

                  newSucursal.direccion = direccion._id;
                  newSucursal.save();
                  // mensaje flash
                  req.flash('success', 'Sucursal creada!');
                  return res.redirect('/empresas/'+ idEmpresa);

                }

              });
            }
          });
        }
      });
    };

    // eliminar sucursal
    var delSucursal = function(req, res) {

      var arr = req.params.id.split(";");
      var idSucursal = arr[1];
      var idEmpresa = arr[0];

      Async.parallel([
        // eliminar direccion de sucursal
        function(callback) {
          // encontrando direccion enlazada a sucursal
          Direccion.remove({ sucursal: idSucursal }, function(err){
              // debugger;
              if(err) {
                callback(err);
              } else {
                callback('ok');
              }
          });
        },
        // eliminar sucursal
        function(callback) {
          Sucursal.findByIdAndRemove(idSucursal, function(err){
            // debugger;
              if(err){
                callback(err);
              } else {
                callback('ok');
              }
          });
        }

      ], function(err, results) {
          // debugger;
          // validacion error en las consultas
          if (err) {

            console.log(err);
            req.flash('error', 'Error de Servidor' + err);
            return res.redirect('/empresas/'+ idEmpresa);

          } else {

            req.flash('success','Sucursal eliminada!');
            return res.redirect('/empresas/'+ idEmpresa);

          }

          // validacion resultados sin datos
          // if (results == null || results[0] == null) {
          //   return res.send(400);
          // }

      });


      // Sucursal.findById(idSucursal, function(err, sucursal){
      //
      //   debugger;
      //
      //   if(err) {
      //
      //   } else {
      //     // idsucursal
      //     var idSucursal = sucursal._id;
      //
      //     Direccion.find({sucursal: idSucursal}, function(err, direccion) {
      //         debugger;
      //         // validacion de error
      //         if(err) {
      //
      //         } else {
      //             direccion.remove(function(err){
      //                 debugger;
      //                 // validacion de error
      //                 if(err) {
      //
      //                 } else {
      //                   sucursal.remove(function(err){
      //                     debugger;
      //                     // se valida error de eliminacion
      //
      //                     if(!err){
      //                       req.flash('success','Sucursal eliminada!');
      //                       return res.redirect('/empresas/'+ idEmpresa);
      //
      //                     } else {
      //                       //mensaje flash
      //                       req.flash('error', 'Error de Servidor' + err);
      //                       return res.redirect('/empresas/'+ idEmpresa);
      //                     }
      //
      //                   });
      //                 }
      //             });
      //         }
      //     });
      //   }

        // eliminando sucursal
        // sucursal.remove(function(err) {
        //
        //
        //
        //   // se valida error de eliminacion
        //   if(!err){
        //     req.flash('success','Sucursal eliminada!');
        //     return res.redirect('/empresas/'+ idEmpresa);
        //
        //   } else {
        //     //mensaje flash
        //     req.flash('error', 'Error de Servidor' + err);
        //     return res.redirect('/empresas/'+ idEmpresa);
        //   }
        //
        // });

      //});
    };

    // retorno de sucursal por empresa
    var loadSucursalesByEmpresa = function(req, res) {
        // id empresa
        var mongoose = require('mongoose');
        var idEmpresa = mongoose.Types.ObjectId(req.body.idEmpresa);/* req.body.idEmpresa; */
        // busqueda de sucursales por filtrando por empresa
        Sucursal.find({ empresa : idEmpresa }, function(err, sucursales) {
            if(!err) {
                var jsonSucursales = JSON.stringify(sucursales);
                res.writeHead(200, {'content-type':'text/json'});
                return res.end(jsonSucursales);
            } else {
                return res.statusCode = 500;
            }
        });
    };

    // api sucursales movil
    var sucursalmovil = function(req, res) {
        // id empresa
        //var id_empresa = req.params.id;
        // debugger;

        Sucursal.find({})
            .populate('direccion')
            .populate('empresa')
            .exec(function(err, sucursales){
            // debugger;
            if(!err) {
              res.jsonp({sucursales: sucursales})
            }
        });

        // var sucursales;
        // var empresas;
        //
        // Sucursal.find({}, function(err, sucursales) {
        //    if(!err) {
        //      Empresa.find({}, function(err, empresas) {
        //        if(!err) {
        //          Direccion.find({}, function(err, direcciones) {
        //             if(!err) {
        //               res.jsonp({
        //                 sucursales: sucursales,
        //                 empresas: empresas,
        //                 direcciones: direcciones
        //               });
        //             }
        //          });
        //        }
        //      });
        //    }
        // });
    };

    // api get sucursales movil by id Empresa
    var sucursalById = function(req, res) {

        // id empresa
        var mongoose = require('mongoose');
        var empresaId = mongoose.Types.ObjectId(req.body.empresaId);

        Async.parallel([
            function(callback) {
              // buscar sucursal por id de empresa
              Sucursal.find({ empresa: empresaId }, function(err, sucursales) {
                  // validacion de error
                  if(err) {
                      callback(err);
                  }
                  callback(err, sucursales);
              });
            },
            function(callback) {
              // Direcciones
              Direccion.find({ }, function(err, direcciones) {
                  // validacion de error
                  if(err) {
                      callback(err);
                  }
                  callback(err, direcciones);
              });
            }
        ],

        function(err, results) {
          // retorno
          if (err) {
            res.jsonp({
                sucursales:    results[0],
                direcciones:   results[1],
                pass: 0,
                message: 'error',
                error: err
            });
          } else {
            res.jsonp({
                sucursales:    results[0],
                direcciones:   results[1],
                pass: 1,
                message: 'success',
                error: err
            });
          }

        });
    };

    // api get direcciones by id Sucursal
    var direccionById = function(req, res) {
        // id sucursal
        var mongoose = require('mongoose');
        var sucursalId = mongoose.Types.ObjectId(req.body.sucursalId);

        // buscar sucursal por id de empresa
        Direccion.find({ sucursal: sucursalId }, function(err, direcciones) {
            // validacion de error
            if(!err) {
                res.jsonp({ direcciones: direcciones, pass: 1, message: 'success' });
            } else {
                res.jsonp({ direcciones: direcciones, pass: 0, message: 'error' });
            }
        });
    };

    //rutas
    // obtener sucursales por id
    app.get('/empresas/getSucursales/:id',isLoggedIn, getSucursales);
    // ins sucursales
    app.post('/empresas/nuevaSucursal', isLoggedIn, ingresoSucursal);
    // eliminar sucursal
    app.get('/empresas/eliminarSucursal/:id',isLoggedIn, delSucursal);
    // cargar sucursales por empresa
    app.post('/empresas/sucursalesbyempresa', isLoggedIn, loadSucursalesByEmpresa);
    // sucursales por empresa movil
    app.get('/sucursalesmovil/', sucursalmovil);
    // api sucursal by id empresa movil
    app.post('/api/getsucursales', sucursalById);

    // middleware para asegurar que el usuario esta logeado a
    // la aplicacion
    function isLoggedIn(req, res, next) {
      // si el usuario esta logeado a la aplicacion
      // continua
      if (req.isAuthenticated())
        return next();

      // si no esta logeado se redirecciona al index de la
      // aplicacion
      res.redirect('/');
    }
};

module.exports = sucursalController;
