var Ejecutivo = require('../models/EjecutivoModel'),
    Perfiles = require('../models/PerfilModel'),
    Empresas = require('../models/EmpresaModel'),
    Sucursales = require('../models/SucursalModel');

var ejecutivoController = function(app, passport){

    // seleccion de ejecutivos para datatable
    var getEjecutivos = function(req, res, next) {

        // opciones de seleccion en datatable
        var options = { select: 'bool', conditions:{ flagEliminado: false } };

        // seleccion de ejecutivos en base de datos
        Ejecutivo.dataTable(req.query, options, function(err, data){
            // validacion de retorno de datos
            if (err) return next(err);

            // retorno de datos
            res.send(data);
        });
    }

    // carga de pagina de ingreso de ejecutivos
    var loadInsEjecutivo = function(req, res){

         // mensaje flash dinamico
         var flash = req.flash();

         // perfiles
         Perfiles.find({ flagEliminado: false }, function(err, perfiles) {
            if(!err){
              // empresas
              Empresas.find({ flagEliminado: false }, function(err, empresas) {
                 if(!err){

                      res.render('ejecutivos/nuevoejecutivo',
                      {
                        flash: flash,
                        empresas: empresas,
                        perfiles: perfiles
                      });
                 }
              });
            }
         });
    };

    // funcion de ingreso de un ejecutivo
    var insertEjecutivo = function(req, res) {

        // nombre
        var nombre = req.body.nombre;
        // apellido
        var apellido = req.body.apellido;
        // rut
        var rut = req.body.rut;
        // email
        var email = req.body.email;
        // usuario
        var usuario = req.body.usuario;
        // contrasena
        var contrasena = req.body.contrasena;
        // fecha creacion
        var fechaCreacion = Date.now();
        // empresa
        var empresa = req.body.empresa;
        // perfil
        var perfil = req.body.perfil;
        //sucursal
        var sucursal = req.body.sucursal;
        // modulo de atencion
        var modulo = req.body.modulo;

        Ejecutivo.findOne({usuario: usuario }, function(err, ejecutivo){

            // error de la consulta a mongodb
            if(err){
                // mensaje de error
                req.flash('error', "Oops! Error!" + err);
                // redireccionando a pagina de ingreso de ejecutivo
                return res.redirect('/nuevoejecutivo');
            }
            // si existe el ejecutivo como usuario no se ingresa
            if(ejecutivo){
                // mensaje de warning
                req.flash('warning', 'Oops! Usuario ya existe dentro sistema');
                // redireccionando a pagina de ingreso de ejecutivo
                return res.redirect('/nuevoejecutivo');
            } else {

                // se ingresa nuevo ejecutivo
                var newEjecutivo = new Ejecutivo();

                    newEjecutivo.nombre = nombre;
                    newEjecutivo.apellido = apellido;
                    newEjecutivo.rut = rut;
                    newEjecutivo.email = email;
                    newEjecutivo.usuario = usuario;
                    newEjecutivo.contrasena = newEjecutivo.generateHash(contrasena);
                    newEjecutivo.fechaCreacion = fechaCreacion;
                    newEjecutivo.flagEliminado = false;
                    newEjecutivo.empresa = empresa;
                    newEjecutivo.perfil = perfil;
                    newEjecutivo.sucursal = sucursal;
                    newEjecutivo.modulo = modulo;

                newEjecutivo.save(function(err){

                   // control de error
                   if(err){

                       // mensaje de error
                       req.flash('error', 'Error! guardando registo: ' + err);
                       // redireccionando a pagina de ingreso de ejecutivo
                       return res.redirect('/nuevoejecutivo');

                   } else {

                       // mensaje de ingreso OK
                       req.flash('success', 'Ingreso Ok');
                       // redireccionando a pagina de ingreso de ejecutivo
                       return res.redirect('/nuevoejecutivo');
                  }

               });
             }
        });
    };

    // listado de ejecutivos
    var loadEjecutivos = function(req, res){

        // retornando todos los ejecutivos
        Ejecutivo.find(function(err, ejecutivos) {
            if(!err) {

              // mensaje flash dinamico
              var flash = req.flash();
              // se retornan todos los ejecutivos
              res.render('ejecutivos/ejecutivos', {ejecutivos: ejecutivos, flash: flash});
            } else {
              res.statusCode = 500;
              //console.log('Error! consultando los datos' + err);
              return res.redirect('/ejecutivos');
            }
        });

    };

    // busqueda de ejecutivos nombre
    var findEjecutivo = function(req, res){

      // usuario ejecutivo
      var usuario = req.body.usuario;

      // retornando un ejecutivo
      return Ejecutivo.find({usuario: usuario}, function(err, ejecutivo){

        // se valida si se puede realizar la consulta
        if(ejecutivo.length == 0){

            // mensaje flash
            req.flash('error', 'No se encontraron ejecutivos con el nombre especificado.');
            return res.redirect('/ejecutivos');
        }

        if(!err){

          // se retorna a ejecutivo encontrado segun el filtro
          res.render('ejecutivos/ejecutivos', {ejecutivos: ejecutivo});

        } else {

          // mensaje flash
          req.flash('error', 'Error! consultando los datos' + err);
          return res.redirect('/ejecutivos');
        }

      });

    };

    // seleccion de ejecutivo id
    var selEjecutivo = function(req, res){

      // buscar datos de ejecutivo
      return Ejecutivo.findById(req.params.id, function(err, ejecutivo){

        // se valida que la consulta no tenga errores
        if(!err){

          // retorna y renderiza a pagina de edicion de ejecutivo
          res.render('ejecutivos/editarejecutivo', {ejecutivo: ejecutivo});

        } else {

          // mensaje de error de busqueda de ejecutivo
          req.flash('error', 'Error! consultando los datos' + err);
          return res.redirect('/ejecutivos');

        } // termino else
      }); // termino funcion
    }; // termino editEjecutivo

    // actualizacion de ejecutivo
    var updEjecutivo = function(req, res){

      // validar id de ejecutivo
      return Ejecutivo.findById(req.body.idEjecutivo, function(err, ejecutivo){

            // nombre
            if(req.body.updNombre != null) {ejecutivo.nombre = req.body.updNombre};
            // apellido
            if(req.body.updApellido != null) {ejecutivo.apellido = req.body.updApellido};
            // rut
            if(req.body.updRut != null) {ejecutivo.rut = req.body.updRut};
            // email
            if(req.body.updEmail != null) {ejecutivo.email = req.body.updEmail};

            return ejecutivo.save(function(err) {

              // se valida si existe error
              if(!err){

                // mensaje flash
                req.flash('success', 'Ejecutivo actualizado!');
                return res.redirect('/ejecutivos');

              } else {

                if(err.name == 'Validation error'){

                  // mensaje flash
                  req.flash('error', 'Error de validacion');
                  return res.redirect('/ejecutivos');

                } else {

                  // mensaje flash
                  req.flash('error','Error de Servidor' + err);
                  return req.redirect('/ejecutivos');

                } // validation error
              } // err
          });
      });
    };

    // eliminacion de ejecutivo
    var delEjecutivo = function(req, res){

      // validar si el usuario existe
      return Ejecutivo.findById(req.params.id, function(err, ejecutivo) {

            // eliminando usuario
            return ejecutivo.remove(function(err){
              console.log('usuario eliminado');

              // se valida error de eliminacion
              if(!err){
                // mensaje flash

                Ejecutivo.find(function(err, ejecutivos){

                  // retornando los ejecutivos restantes
                  if(!err){
                    req.flash('success','Ejecutivo eliminado!');
                    return res.redirect('/ejecutivos');
                  } else {
                    req.flash('error','Error consultando ejecutivos!');
                    return res.redirect('/ejecutivos');
                  }

                });

              } else {
                //mensaje flash
                req.flash('error', 'Error de Servidor' + err);
                return res.redirect('/ejecutivos');
              }

            });

      });
    };

    // rutas

    // selecciona todos los ejecutivos para datatable
    app.get('/getejecutivos', getEjecutivos);

    // ingreso a la pagina de nuevo ejecutivo
    app.get('/nuevoejecutivo', isLoggedIn, loadInsEjecutivo);

    // nuevo ejecutivo
    app.post('/nuevoejecutivo', isLoggedIn, insertEjecutivo);

    // seleciona todos los ejecutivos
    app.get('/ejecutivos', isLoggedIn, loadEjecutivos);

    // actualizar ejecutivo
    app.post('/updejecutivo', isLoggedIn, updEjecutivo);

    // eliminar ejecutivo
    app.get('/eliminarejecutivo/:id',isLoggedIn, delEjecutivo);

    // middleware para asegurar que el usuario esta logeado a
    // la aplicacion
    function isLoggedIn(req, res, next) {
      // si el usuario esta logeado a la aplicacion
      // continua
      if (req.isAuthenticated())
        return next();

      // si no esta logeado se redirecciona al index de la
      // aplicacion
      res.redirect('/');
    }
};

module.exports = ejecutivoController;
