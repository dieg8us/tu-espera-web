var Horario = require('../models/HorarioModel'),
    DetalleHorario = require('../models/DetalleHorarioModel');

var HorarioController = function(app, passport) {

    // funcion que rescata todos los horarios creados
    var gethorarios = function(req, res){

        var options = { select: 'bool', conditions:{ flagEliminado: false } };

        Horario.dataTable(req.query, options, function(err, data){
            if (err) return next(err);
            res.send(data);
        });
    };

    var loadInsHorario = function(req, res) {

        // mensajes flash
        var flash = req.flash();

        Horario.find({ flagEliminado: false}, function(err, horarios) {
            if(!err){
                res.render('horarios/horarios', { flash: flash, horarios: horarios });
            } else {
              res.redirect('/home');
            }
        });
    };

    var insHorario = function(req, res){

        var nombre;
        var mes;

        if(req.body.nom_horario != null){ nombre = req.body.nom_horario };
        if(req.body.mes != null){ mes = req.body.mes };

        Horario.findOne({ nombre: nombre}, function(err, horario) {
            // validacion si existe el horario
            if(!err){
              // validacion de horario
              if(horario){
                // mensajes flash
                req.flash('error','Opps nombre de horario ya ingresado!');
                // redireccionando a pagina de horarios
                res.redirect('/horarios');
              } else {

                  // se ingresa detalle de horario

                  // variables inicio detalle horario
                  var iniLunes;
                  var iniMartes;
                  var iniMiercoles;
                  var iniJueves;
                  var iniViernes;
                  var iniSabado;
                  var iniDomingo;

                  // lunes
                  if(req.body.inicio_lunes != null ){
                      iniLunes = req.body.inicio_lunes
                  };

                  // martes
                  if(req.body.inicio_martes != null ){
                    iniMartes = req.body.inicio_martes
                  };

                  // miercoles
                  if(req.body.inicio_miercoles != null ){
                    iniMiercoles = req.body.inicio_miercoles
                  };

                  // jueves
                  if(req.body.inicio_jueves != null ){
                    iniJueves = req.body.inicio_jueves
                  };

                  // viernes
                  if(req.body.inicio_viernes != null ){
                    iniViernes = req.body.inicio_viernes
                  };

                  // sabado
                  if(req.body.inicio_sabado != null ){
                    iniSabado = req.body.inicio_sabado
                  };

                  // domingo
                  if(req.body.inicio_domingo != null ){
                    iniDomingo = req.body.inicio_domingo
                  };

                  // variables termino detalle horario
                  var terLunes;
                  var terMartes;
                  var terMiercoles;
                  var terJueves;
                  var terViernes;
                  var terSabado;
                  var terDomingo;

                  // lunes
                  if(req.body.termino_lunes != null ){
                    terLunes = req.body.termino_lunes
                  };

                  // martes
                  if(req.body.termino_martes != null ){
                    terMartes = req.body.termino_martes
                  };

                  // miercoles
                  if(req.body.termino_miercoles != null ){
                    terMiercoles = req.body.termino_miercoles
                  };

                  // jueves
                  if(req.body.termino_jueves != null ){
                    terJueves = req.body.termino_jueves
                  };

                  // viernes
                  if(req.body.termino_viernes != null ){
                    terViernes = req.body.termino_viernes
                  };

                  // sabado
                  if(req.body.termino_sabado != null ){
                    terSabado = req.body.termino_sabado
                  };

                  // domingo
                  if(req.body.termino_domingo != null ){
                    terDomingo = req.body.termino_domingo
                  };

                  var newDetalleHorario = new DetalleHorario();

                  newDetalleHorario.inicioLunes = iniLunes;
                  newDetalleHorario.inicioMartes = iniMartes;
                  newDetalleHorario.inicioMiercoles = iniMiercoles;
                  newDetalleHorario.inicioJueves = iniJueves;
                  newDetalleHorario.inicioViernes = iniViernes;
                  newDetalleHorario.inicioSabado = iniSabado;
                  newDetalleHorario.inicioDomingo = iniDomingo;
                  newDetalleHorario.terminoLunes = terLunes;
                  newDetalleHorario.terminoMartes = terMartes;
                  newDetalleHorario.terminoMiercoles = terMiercoles;
                  newDetalleHorario.terminoJueves = terJueves;
                  newDetalleHorario.terminoViernes = terViernes;
                  newDetalleHorario.terminoSabado = terSabado;
                  newDetalleHorario.terminoDomingo = terDomingo;
                  newDetalleHorario.fechaCreaciion = Date.now();
                  newDetalleHorario.flagEliminado = false;

                  newDetalleHorario.save(function(err, detalle) {
                      // validacion de ingreso de datos
                      if(!err){
                        // se ingresa horario nuevo
                        var newHorario = new Horario();

                        newHorario.nombre = nombre;
                        newHorario.mes = mes;
                        newHorario.fechaCreaciion = Date.now();
                        newHorario.usuario = req.user.id;
                        newHorario.detalle = detalle.id;
                        newHorario.flagEliminado = false;

                        newHorario.save(function(err, save) {
                            // validacion de ingreso
                            if(!err){
                              // mensaje de ingreso de horario
                              req.flash('success','Ingreso de horario satisfactorio');
                              // redireccionando a pagina de horarios
                              res.redirect('/horarios');
                            } else {
                              // mensaje de ingreso de horario
                              req.flash('error','Error en el ingreso de horario');
                              // redireccionando a pagina de horarios
                              res.redirect('/horarios');
                            }
                        });

                      } else {
                        // eliminando registro que se ingreso como cabecera
                        detalle.remove();
                        // error en el ingreso de detalle de horario
                        req.flash('error','Error en el ingreso de detalle de horario' + err);
                        // redireccionando a pagina de ingreso de horarios
                        res.redirect('/horarios');
                      }
                  });
                }
            }
        });
    };

    var loadEditHorario = function(req, res) {
        // mensajes flash
        var flash = req.flash();
        // renderizando pagina de edicion de horarios
        res.render('horarios/editarhorarios',{ flash: flash});
    };

    var EditHorario = function(req, res) {
        // edicion del horario ingresado
        var idHorario = req.body.idHorario;

        //identificando horario
        Horario.findById(idHorario, function(err, horario) {
            // validacion de la consulta
            if(!err){
              // validando que si el horario existe
              if(horario){

                  var nombre;
                  var mes;

                  var iniLunes;
                  var iniMartes;
                  var iniMiercoles;
                  var iniJueves;
                  var iniViernes;
                  var iniSabado;
                  var iniDomingo;

                  var terLunes;
                  var terMartes;
                  var terMiercoles;
                  var terJueves;
                  var terViernes;
                  var terSabado;
                  var terDomingo;

                  // cabecera horario
                  if(req.body.updNombre != null){ nombre = req.body.updNombre };
                  if(req.body.updMes != null){ mes = req.body.updMes };
                  // detalle horario

                  // inicio
                  if(req.body.updInicioLunes != null){ iniLunes = req.body.updInicioLunes };
                  if(req.body.updInicioMartes != null){ iniMartes = req.body.updInicioMartes };
                  if(req.body.updInicioMiercoles != null){ iniMiercoles = req.body.updInicioMiercoles };
                  if(req.body.updInicioJueves != null){ iniJueves = req.body.updInicioJueves };
                  if(req.body.updInicioViernes != null){ iniViernes = req.body.updInicioViernes };
                  if(req.body.updInicioSabado != null ){ iniSabado = req.body.updInicioSabado };
                  if(req.body.updInicioDomingo != null ){ iniDomingo = req.body.updInicioDomingo };

                  // termino
                  if(req.body.updTerminoLunes != null){ terLunes = req.body.updTerminoLunes };
                  if(req.body.updTerminoMartes != null){ terMartes = req.body.updTerminoMartes };
                  if(req.body.updTerminoMiercoles != null){ terMiercoles = req.body.updTerminoMiercoles };
                  if(req.body.updTerminoJueves != null){ terJueves = req.body.updTerminoJueves };
                  if(req.body.updTerminoViernes != null){ terViernes = req.body.updTerminoViernes };
                  if(req.body.updTerminoSabado != null ){ terSabado = req.body.updTerminoSabado };
                  if(req.body.updTerminoDomingo != null ){ terDomingo = req.body.updTerminoDomingo };

                  DetalleHorario.findOne({ detalle: horario.id }, function(detalleHorario, err){
                      if(!err){
                        // validando que exista el detalle de horario
                        if(detalleHorario){

                          detalleHorario.inicioLunes = iniLunes;
                          detalleHorario.inicioMartes = iniMartes;
                          detalleHorario.inicioMiercoles = iniMiercoles;
                          detalleHorario.inicioJueves = iniJueves;
                          detalleHorario.inicioViernes = iniViernes;
                          detalleHorario.inicioSabado = iniSabado;
                          detalleHorario.inicioDomingo = iniDomingo;

                          detalleHorario.terminoLunes = terLunes;
                          detalleHorario.terminoMartes = terMartes;
                          detalleHorario.terminoMiercoles = terMiercoles;
                          detalleHorario.terminoJueves = terJueves;
                          detalleHorario.terminoViernes = terViernes;
                          detalleHorario.terminoSabado = terSabado;
                          detalleHorario.terminoDomingo = terDomingo;

                          // actualizando detalle de horario
                          detalleHorario.save(function(err){
                            if(!err){
                              // actualizando horario
                              horario.save(function(err){
                                if(!err){
                                  // mensaje flash
                                  req.flash('success', 'horario actualizado');
                                  // redireccionando a pagina de edicion de horario
                                  res.redirect('/edithorarios');
                                }
                              });
                            }
                          });
                        }

                      } else {
                        // mensaje flash
                        req.flash('error', 'Error en la consulta de detalle' + err);
                        // redireccionando a pagina de edicion de horarios
                        res.redirect('/edithorarios');

                      }
                  });


              } else {
                // mensaje flash
                req.flash('error', 'horario no existe en la base de datos');
                // redireccionando a pagina de edicion de horarios
                res.redirect('/edithorarios');
              }

            } else {
              // mensaje flash
              req.flash('error', 'error en la consulta' + err);
              // redireccionando
              res.redirect('/edithorarios');
            }
        });
    };

    //var insertHorario = function(req, res) {

        // variables de ingreso

    //};

    // rutas

    // horarios datatable
    app.get('/gethorarios', gethorarios);
    // ingreso a pagina de horarios
    app.get('/horarios', loadInsHorario);
    // ingreso de horarios
    app.post('/horarios', insHorario);
    // ingreso editar horarios
    app.get('/edithorarios', loadEditHorario);
};

module.exports = HorarioController;
